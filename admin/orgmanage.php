<?php 	require_once("../includes/function.php");  require_once("../includes/template2.php");  
show_header("Organization manage"); show_menu(); check_authen("Admin");
require_once("../includes/paginator1.php");	 
?>

<script language="javascript">
function check_uncheck_all(checkname, field) {
	for (i = 0; i < checkname.length; i++) {
		checkname[i].checked = field.checked? true:false
}
}
</script>

<SCRIPT language="JavaScript">
<!--   
  function Conf(object) {
  if (confirm("Press OK to confirm delete\n Press Cancel to cancel") == true) {
  return true;
  }
  return false;
  }
//-->
</SCRIPT>

<hr width='98%' color='blue'> 

<table cellpadding="1" cellspacing="0" width="98%" border="0" style="font-weight:bold; color:#666666">  
<form name="frmSearch" method="get" action="<?php echo $_SERVER['SCRIPT_NAME'];?>"> 
<tr bgcolor="lightgreen" valign="bottom"><td><font size='-1'>&nbsp;<input name="txtKeyword" type="text" id="txtKeyword" value=""> 
<input type="submit" name="search" id="search" value="Search">&nbsp;&nbsp;<input type="submit" name="reset" id="reset" value="Reset">&nbsp;&nbsp;
<a href="orgnew.php"><IMG SRC="../images/add_new_button.jpg" WIDTH="103" HEIGHT="25" BORDER="0" vspace="0" align="middle" ALT="Add New Organization"></a></td></tr>
</form> 
</table>

<?php
$tbname="org";

if (($_GET["reset"]=="Reset") or (!empty($_GET["sql"]))){
	$_SESSION["sql"]='';
}

if (empty($_GET["search"])){	
	if($_SESSION["sql"]!=''){
		$strSQL=$_SESSION["sql"];
}else{
		$strSQL="SELECT * FROM $tbname";
		$strSQL .=" order by name ASC";
	}
}

if($_GET["txtKeyword"] != "") 
{
	$strSQL = "SELECT * FROM $tbname WHERE ((sec_id LIKE '%".$_GET["txtKeyword"]."%') or  (name LIKE '%".$_GET["txtKeyword"]."%') or (dep_name LIKE '%".$_GET["txtKeyword"]."%') or (div_name LIKE '%".$_GET["txtKeyword"]."%') or (institute_name LIKE '%".$_GET["txtKeyword"]."%'))"; 
	$strSQL .=" order by name ASC";

}
$_SESSION["sql"]=$strSQL;

$dblink = connect_db() ;
$reSult = mysqli_query( $dblink, $strSQL ) ;
@$Num_Rows = mysqli_num_rows( $reSult ); 

/*****Insert page find record per page********/
require_once("../includes/paginator2.php");
/*****End Insert page find record per page********/

$strSQL .=" LIMIT $Page_Start, $Per_Page";
$reSult = mysqli_query( $dblink, $strSQL );
@$numRows = mysqli_num_rows( $reSult ); 

if ( $numRows == 0 ) {  			
	echo "<font size=\"+1\">No data .!.</font><br/><br/>" ; 
	show_footer();  	 
	exit; 
}else{ 
?>
	<table cellpadding="1" cellspacing="2" width="98%" border="0" bgcolor="#FFCC00" style="font-weight:bold; color:#666666">  
        <tr bgcolor="#A2374D" style="color:#FFFFFF" align="center"><td colspan="10" height="30">Organization Management</td></tr>
		<tr bgcolor="#CCCC99" style="color:#000000" align="center">
             <td height="30">ลำดับ<br>item</td>
			 <td>รหัสย่อแผนก<br>Section id</td>
			 <td>ชื่อย่อแผนก<br>Name</td> 
			 <td>ชื่อแผนก<br>Section detail</td> 
			  <td>ชื่อย่อกอง<br>Department</td> 
			 <td>ชื่อย่อฝ่าย<br>Division</td> 
			 <td>ชื่อย่อส่วน<br>Institute</td> 

			 <td>Delete</td>
			</tr>
        </tr>

<?php
		if($Page==1){
			$i=1;
		}else{
			$i=($Page-1)*$Per_Page+1;
		}

		$url="orgmanage.php";
        while ( $row = mysqli_fetch_array( $reSult, MYSQL_ASSOC ) ) {
				$bgc = ($bgc=="#FAF2E6") ? "#FAE6E6" : "#FAF2E6";
				echo '<tr bgcolor="'.$bgc.'" height="20">';
?>
				<td width="7%" align="center"><A HREF="orgselect.php?id=<?php echo $row['id']?>"><?php echo $i ; ?></a></td>
				<td align="center" width="8%"><font color='blue'><?php echo $row["sec_id"] ; ?></font></td>
				<td align="left" width="10%"><font color='blue'>&nbsp;<?php echo $row["name"] ; ?></font></td>
				<td align="left" width="15%"><font color='blue'>&nbsp;<?php echo $row["sec_detail"] ; ?></font></td>
				<td align="center" width="8%"><font color='blue'><?php echo $row["dep_name"] ; ?></font></td>
				<td align="center" width="8%"><font color='blue'><?php echo $row["div_name"] ; ?></font></td>
				<td align="center" width="8%"><font color='blue'><?php echo $row["institute_name"] ; ?></td>				
<?php 	
				echo '<td width="10%" align="center"><font size="1">&nbsp;<a href="delrecord.php?id='.$row['id'].'&tb='.$tbname.'&url='.$url.'" onClick="return Conf(this)">Delete</a></td></tr>';
				$i++ ;
			} 
		mysqli_close($dblink);
		$sql ='';
		//echo '<tr bgcolor="#66FFCC"><td align="center">Edit</td><td colspan="6" align="right">&nbsp;</td>';
		//echo '<td align="center"><input type="submit" name="Submit" value="Delete" /></td></tr></form></table>';	
		echo '</form></table>';
} 
?>

<table border='0' cellspacing='0' width='98%'>
<tr align="left" bgcolor="lightgreen">
<td colspan="7" height='25'>Total <?php echo $Num_Rows;?> Record

<!--*****Insert Show page navigator********-->
<?php require_once("../includes/paginator3.php"); ?>
<!--*****End Insert Show page navigator********-->

</td></tr></table>
<hr width='98%' color='blue'> 

<?php	show_footer();?>