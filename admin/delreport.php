<?php 	require_once("../includes/function.php");  require_once("../includes/template2.php");  
show_header("Delete Data Report"); show_menu(); check_authen("Admin");
require_once("../includes/paginator1.php");	 
?>
<hr width='70%' color='blue'> 
<table cellpadding="1" cellspacing="0" width="70%" border="0" style="font-weight:bold; color:#666666">  
<form name="frmSearch" method="get" action="<?php echo $_SERVER['SCRIPT_NAME'];?>"> 
<tr bgcolor="lightgreen"><td><font size='-1'>&nbsp;<input name="txtKeyword" type="text" id="txtKeyword" value=""> 
<input type="submit" name="search" id="search" value="Search">&nbsp;&nbsp;<input type="submit" name="reset" id="reset" value="Reset"></td></tr>
</form> 
</table>

<?php
$tbname="delreport";

if (($_GET["reset"]=="Reset") or (!empty($_GET["sql"]))){
	$_SESSION["sql"]='';
}

if (empty($_GET["search"])){	
	if($_SESSION["sql"]!=''){
		$strSQL=$_SESSION["sql"];
}else{
		$strSQL="SELECT * FROM $tbname";
		$strSQL .=" order by id DESC";
	}
}

if($_GET["txtKeyword"] != "") 
{
	$strSQL = "SELECT * FROM $tbname WHERE ((tbname LIKE '%".$_GET["txtKeyword"]."%') or (idnumber LIKE '%".$_GET["txtKeyword"]."%') or (rec1 LIKE '%".$_GET["txtKeyword"]."%') or (rec2 LIKE '%".$_GET["txtKeyword"]."%') or (rec3 LIKE '%".$_GET["txtKeyword"]."%'))"; 
	$strSQL .=" order by id DESC";
}
$_SESSION["sql"]=$strSQL;

$dblink = connect_db() ;
$reSult = mysqli_query( $dblink, $strSQL ) ;
$Num_Rows = @mysqli_num_rows( $reSult ); 

/*****Insert page find record per page********/
require_once("../includes/paginator20.php");
/*****End Insert page find record per page********/

$strSQL .=" LIMIT $Page_Start, $Per_Page";
$reSult = mysqli_query( $dblink, $strSQL );
$numRows = @mysqli_num_rows( $reSult ); 

if ( $numRows == 0 ) {  			
	echo "<font size=\"+1\">No data .!.</font><br/><br/>" ; 
	show_footer();  	 
	exit; 
}else{ 
?>
	<table cellpadding="1" cellspacing="2" width="70%" border="0" bgcolor="#FFCC00" style="font-weight:bold; color:#666666">  
        <tr bgcolor="#A2374D" style="color:#FFFFFF" align="center"><td colspan="9" height="30">Delete Data Report</td></tr>
		<tr bgcolor="#CCCC99" style="color:#000000" align="center">
             <td height="30">Item</td>
			 <td>Table Name</td> 
			 <td>ID Number</td>
			 <td>Record1</td>
			 <td>Record2</td>
			 <td>Record3</td>
			 <td>Delete By</td>
			 <td>Delete Time</td>
			 <td>From PC IP</td>
			</tr>
        </tr>

<?php
		if($Page==1){
			$i=1;
		}else{
			$i=($Page-1)*$Per_Page+1;
		}

        while ( $row = mysqli_fetch_array( $reSult, MYSQL_ASSOC ) ) {
				$bgc = ($bgc=="#FAF2E6") ? "#FAE6E6" : "#FAF2E6";
				echo '<tr bgcolor="'.$bgc.'" height="20">';
?>
				<td width="7%" align="center"><?php echo $i ; ?></td>
				<td align="left" width="10%"><font color='blue'>&nbsp;<?php echo $row["tbname"] ; ?></font></td>
				<td align="left" width="10%"><font color='blue'>&nbsp;<?php echo $row["idnumber"] ; ?></font></td>
				<td align="left" width="15%"><font color='blue'>&nbsp;<?php echo $row["rec1"] ; ?></font></td>
				<td align="left" width="15%"><font color='blue'>&nbsp;<?php echo $row["rec2"] ; ?></font></td>
				<td align="left" width="15%"><font color='blue'>&nbsp;<?php echo $row["rec3"] ; ?></font></td>
				<td align="left" width="10%"><font color='blue'>&nbsp;<?php echo $row["delby"] ; ?></font></td>
				<td align="left" width="15%"><font color='blue'>&nbsp;<?php echo $row["deltime"] ; ?></font></td>
				<td align="left" width="10%"><font color='blue'>&nbsp;<?php echo $row["delip"] ; ?></font></td></tr>
			
<?php 	
				$i++ ;
			} 
		echo '</table>';
		mysqli_close($dblink);
		$sql ='';
} 
?>

<table border='0' cellspacing='0' width='70%'>
<tr align="left" bgcolor="lightgreen">
<td colspan="9" height='25'>Total <?php echo $Num_Rows;?> Record

<!--*****Insert Show page navigator********-->
<?php require_once("../includes/paginator3.php"); ?>
<!--*****End Insert Show page navigator********-->

</td></tr></table>
<hr width='70%' color='blue'> 

<?php	show_footer();?>