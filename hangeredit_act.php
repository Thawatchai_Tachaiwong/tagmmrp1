
<?php  /**By Anek suriwongyai 09-06-2562 */
    session_start();
    // require_once("includes/function.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<script src="./js/jquery.min.js"></script>

<link rel="stylesheet" href="./css/bootstrap.min.css" media="all">
<link rel='stylesheet' type='text/css' href='./css/style.css'>
<title>Edit Hanger</title>
<link rel="stylesheet" type="text/css" href="dist/jquery.dataTables.min.css"/>	 
<script type="text/javascript" src="dist/jquery.dataTables.min.js"></script>

<style media="all" type="text/css">
	.round_image{
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		-webkit-box-shadow: #000 0 2px 10px;
		-moz-box-shadow: #000 0 2px 10px;
		box-shadow: #000 0 2px 10px;
	}
</style>

<style>
	@media (min-width: 500px) {
			.container{
				width: 800px;
                align-content: center;
			}
	}
</style>
 
</head>
<body>

<?php
    require_once("navbar_index.php");

    $tbname="hanger";
    $url="hanger.php";
    $edituser=$_SESSION["EGATID"];
    $gdate=date("Y-m-d H:i:s");
    $clientip=$_SERVER["REMOTE_ADDR"];
    if($clientip=="::1"){
        $clientip="127.0.0.1";
    }

    if(ISSET($_POST['submit'])){
        $update = "UPDATE $tbname SET egat_id = '".$_POST["egat_id"]."', name = '".$_POST["name"]."', section_id = '".$_POST["section_id"]."', section_thai = '".$_POST["section_thai"]."', status = '".$_POST["status"]."', phone = '".$_POST["phone"]."', note = '".$_POST["note"]."', editby='".$edituser."', edittime='".$gdate."', editip='".$clientip."' WHERE id ='".$_POST["id"]."'";
        // echo "\$update =".$update.'<br>';
        include_once("connect_db.php");
        $mysqli->query("SET NAMES 'utf8'");
        $result = $mysqli->query($update);
        $affected_rows = $mysqli->affected_rows;
        $mysqli->close();

        if ( @$affected_rows == 1 ) {
            $msg="Update data successful...";
            echo '<center><br /><br /><table width="60%" border="0" cellspacing="0"><tr bgcolor="#eee" height="110"><td align="center">';
            echo '<br/><font face="MS Sans Serif"><font color="red" size="+1">'.$msg.'</font></font><br/>' ;
            echo '<img src="images/process.gif"/><br/>' ;	
            echo '<meta http-equiv="refresh" content="2; URL='.$url.'">' ;
            echo '</tr></td></table></center>';
        }else{
            $msg="Can not update data...!";
            echo '<center><br /><br /><table width="40%" border="0" cellspacing="0"><tr bgcolor="#eee" height="110"><td align="center">';
            echo '<br/><font face="MS Sans Serif"><font color="red" size="+1">'.$msg.'</font></font><br/>' ;
            echo '<img src="images/process.gif"/><br/>' ;	
            echo '<meta http-equiv="refresh" content="2; URL='.$url.'">' ;
            echo '</tr></td></table></center>';
        }
    }
?>

</body>
</html>