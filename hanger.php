<?php   /**By Anek suriwongyai 01-05-2562 */
	session_start();
	require_once("getowner.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<script src="./js/jquery.min.js"></script>
<link rel="stylesheet" href="./css/bootstrap.min.css" media="all">
<link rel='stylesheet' type='text/css' href='./css/style.css'>

<title>Hanger user</title>

<link rel="stylesheet" type="text/css" href="dist/jquery.dataTables.min.css"/>	 
<script type="text/javascript" src="dist/jquery.dataTables.min.js"></script>

<style media="all" type="text/css">
	.round_image{
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		-webkit-box-shadow: #000 0 2px 10px;
		-moz-box-shadow: #000 0 2px 10px;
		box-shadow: #000 0 2px 10px;
	}
</style>
<style>
	@media (min-width: 1500px) {
			.container{
				width: 1600px;
			}
	}
</style>
  
</head>
<body>

<?php require_once("navbar_index.php");?>
<div class="container px-5 p-0">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 mx-auto">
			<table class="table table-hover" id="data_grid" border=0>
		<!--<table id="data_grid" class="display" cellspacing="0">-->
        <thead>
					<tr>
						<th colspan="5"><span style="background-color:yellow">&nbsp;Tag Hanger</span></th>
					</tr>
                    <tr>
						<th>ITEM <mark><a href="hanger_new.php" target="_blnk">[ADD]</a></mark></th>
						<th>EGAT ID</th>
						<th>Name</th>
						<th>SECTION</th>
						<th>Note</th>
					</tr>

        </thead>
    	</table>
    </div>
  </div>


<script type="text/javascript">
	$( document ).ready(function() {
		$('#data_grid').DataTable({
			"bProcessing": true,
			"serverSide": true,
			"ajax":{
				url :"hanger_response.php", // json datasource
				type: "post",  // type of method  ,GET/POST/DELETE
				error: function(){
					$("#data_grid_processing").css("display","none");
				}
			}
		});   
	});
</script>
<br>***Click ITEM number to edit.
</div>
</body>
</html>