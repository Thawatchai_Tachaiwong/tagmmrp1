<?php   /**By Anek suriwongyai 11-05-2562 */
	session_start();
	require_once("getowner.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<script src="./js/jquery.min.js"></script>

<link rel="stylesheet" href="./css/bootstrap.min.css" media="all">
<link rel='stylesheet' type='text/css' href='./css/style.css'>
<title>Hanger Add</title>
<link rel="stylesheet" type="text/css" href="dist/jquery.dataTables.min.css"/>	 
<script type="text/javascript" src="dist/jquery.dataTables.min.js"></script>

<style media="all" type="text/css">
	.round_image{
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		-webkit-box-shadow: #000 0 2px 10px;
		-moz-box-shadow: #000 0 2px 10px;
		box-shadow: #000 0 2px 10px;
	}
</style>
<style>
	@media (min-width: 1500px) {
		.container{
			width: 1600px;
		}
	}
</style>

<SCRIPT language="JavaScript">
	function Conf(object) {
		if (confirm("Press OK to confirm delete\n Press Cancel to cancel") == true) {
			return true;
		}
		return false;
	}
</SCRIPT>


<style>
	html {
		font-size: 1rem;
	}

	@include media-breakpoint-up(sm) {
		html {
			font-size: 1.2rem;
		}
	}

	@include media-breakpoint-up(md) {
		html {
			font-size: 1.4rem;
		}
	}

	@include media-breakpoint-up(lg) {
		html {
			font-size: 1.6rem;
		}
	}

</style>

</head>

<body>
<?php require_once("navbar_index.php"); ?>

<br>
<div class="container px-5 p-0">	
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 mx-auto">
			<div class="d-inline p-0 bg-primary text-white text-center">EDIT HANGER</div>
			<form name="form" method="post" action="hangernew_act.php">
				<table class="table" id="data_grid" border=0>
					<thead>
						<tr bgcolor="#CCCC99" style="color:#000000" align="center">
							<td width="10%"><small>EGAT ID</small></td>
							<td width="20%"><small>Name</small></td>
							<td width="10%"><small>Section ID</small></td>
							<td width="10%"><small>Section Name</small></td>
							<td width="10%"><small>Work Group</small></td>
							<td width="10%"><small>Phone</small></td>
							<td width="15%"><small>Note</small></td>
						</tr>
					</thead>

					<tbody>	
						<tr>
							<td align="center" valign="center"><font size="1"><input type="text" name="egat_id" id="egat_id" size="10"></td>

							<td align="center" valign="center"><font size="1"><input type="text" name="name" id="name" size="25"></td>

							<td align="center" valign="center"><font size="1">
                                <select name="section_id" id="section_id">
                                    <?php
                                        require_once("connect_db.php");
                                        $strSQL="SELECT sec_id, name, dep_name FROM org ORDER BY sec_id ASC;";
                                        $result=$mysqli->query($strSQL);
                                        while($rows=$result->fetch_assoc()){
                                            echo '<option value="'.$rows['sec_id'].'">'.$rows['sec_id'].' : '.$rows['name'].'</option>';	
                                        }
                                    ?>
                                </select>
                            </td>

							<td align="center" valign="center"><font size="1">
                                <select name="section_thai" id="section_thai">
                                    <?php
                                        $strSQL="SELECT name, dep_name FROM org ORDER BY sec_id ASC;";
                                        $result=$mysqli->query($strSQL);
                                        while($rows=$result->fetch_assoc()){
                                            echo '<option value="'.$rows['name'].'">'.$rows['name'].' '.$rows['dep_name'].'</option>';	
                                        }
                                    ?>
                                </select>
                            </td>

                            <td align="center" valign="center"><font size="1">
                                <select name="status" id="status">
                                    <option value="O">Operator</option>
                                    <option value="M">Maintenance</option>
                                </select>
                            </td>

							<td align="center" valign="center"><font size="1"><input type="text" name="phone" id="phone" size="10"></td>

							<td align="center" valign="center"><font size="1"><input type="text" name="note" id="note" size="20"></td>
						</tr>

						<tr>
							<td colspan="12" align=center><input type="submit" name="submit" id="submit" value="Submit">
							<input type="reset" name="btnCancel" value="Reset"></td>
						</tr>
					</tbody>
				</table>
			</form>
    	</div>
  	</div>
</div>
</body>
</html>