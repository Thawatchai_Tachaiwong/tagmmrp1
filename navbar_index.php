<?php   /**By Anek suriwongyai 01-05-2562 */ error_reporting (E_ALL ^ E_NOTICE);?>
<div class="jumbotron">
	<div class="container text-center mt-0">
		<div class="col-md-6 col-lg-6">
			<span>
				<?php echo '<h2>'.$_SESSION["location"].'</h2><h4>'.$_SESSION["owner"].'</h4>'; ?>
			</span>
		</div>
		<div class="col-md-6 col-lg-6">
			<span>
				<img src="./images/loto_logo2.png" width="148" height="100" class="round_image"><br /><?php if(isset($_SESSION["username"])){ echo '<mark>'.$_SESSION["username"].'</mark>';} ?>
			</span>
		</div>
	</div>
</div>

<nav class="navbar navbar-inverse bg-light">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
	  <span style="padding: 0px 5px;">
			<a href="index.php"><button style="padding: 14px 14px" type="button" class="btn btn-info btn-sm">HOME</button></a>
		</span> 
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
		<ul class="nav navbar-nav">
			<?php
			//Admin 
				if(isset($_SESSION["username"]) && $_SESSION["level"]=="99"){
			?>
				<li><a href="./doc/owner/">Owner</a></li>
				<li><a href="./doc/organize/">Organization</a></li>
				<li><a href="./doc/taguser/">User manager</a></li>
				<li><a href="./doc/taghanger/">Hanger manager</a></li>
				<li><a href="./doc/equipment/">Equipment manager</a></li>
			<?php
				}
				//Tag manager
				if(isset($_SESSION["username"]) && $_SESSION["level"]=="55"){
			?>
				<li><a href="./doc/taguser/">User manager</a></li>
				<li><a href="./doc/taghanger/">Hanger manager</a></li>
				<li><a href="./doc/equipment/">Equipment manager</a></li>
			<?php
				}

			//Operator
			if(isset($_SESSION["username"]) && $_SESSION["level"]=="66"){
			?>
				<li><a href="newtag.php">ออก Tag</a></li>
				<li><a href="tailtag.php">ประกบ Tag</a></li>
				<li><a href="edittag.php">แก้ไข Tag</a></li>
				<li><a href="printtag.php">Print Tag</a></li>
				<li><a href="printmf.php" target="_blnk">Print Tag Form</a></li>
				<li><a href="confirmhang.php">ยืนยันแขวน Tag</a></li>
				<li><a href="confirmtagout.php">ยืนยันปลด Tag</a></li>
				<li><a href="reportmf.php" target="_blnk">รายงาน Tag</a></li>
			<?php
			}
			//Maintenance
			if(isset($_SESSION["username"]) && $_SESSION["level"]=="33"){
			?>
				<li><a href="newtag.php">ออก Tag</a></li>
				<li><a href="edittag.php">แก้ไข Tag</a></li>
				<li><a href="printtag.php">Print Tag</a></li>
				<li><a href="reportmf.php" target="_blnk">รายงาน Tag</a></li>
			<?php
			}
			?>
			<li><a href="equipment.php">ค้นหาอุปกรณ์</a></li>
			<li><a href="./doc/tagmanual.pdf">Tag manual</a></li>
			<?php 
			if(!isset($_SESSION["username"])){				
				echo '<li><a href="login.php">Logon</a></li>';
			}else{
				echo '<li><a href="hanger.php">Hanger</a></li>';
				echo '<li><a href="psw_edit.php">Change password</a></li>';
				echo '<li><a href="logout.php">Log out</a></li>';
			}
			?>
	
		</ul>
    </div>
  </div>
</nav>
