<?php   /**By Anek suriwongyai 11-05-2562 */
	session_start();
	require_once("getowner.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<script src="./js/jquery.min.js"></script>

<link rel="stylesheet" href="./css/bootstrap.min.css" media="all">
<link rel='stylesheet' type='text/css' href='./css/style.css'>
<title>Edit tag</title>
<link rel="stylesheet" type="text/css" href="dist/jquery.dataTables.min.css"/>	 
<script type="text/javascript" src="dist/jquery.dataTables.min.js"></script>

<style media="all" type="text/css">
	.round_image{
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		-webkit-box-shadow: #000 0 2px 10px;
		-moz-box-shadow: #000 0 2px 10px;
		box-shadow: #000 0 2px 10px;
	}
</style>
<style>
	@media (min-width: 1500px) {
		.container{
			width: 1600px;
		}
	}
</style>

<!--*********Start calendar************-->
<link type="text/css" href="jquery/flora.calendars.picker.css" rel="stylesheet"/> 
<script type="text/javascript" src="jquery/jquery.min.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.min.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.plus.min.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.picker.min.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.thai.min.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.thai-th.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.picker-th.js"></script> 

<script language="javascript">
	function checkdate_click(){
			if(document.getElementById('checkdate').checked==true){
					document.getElementById('submit').disabled = false;
			}else{
					document.getElementById('Submit').disabled = true;
			}	
	}
</script>

<SCRIPT language="JavaScript">
	function Conf(object) {
		if (confirm("Press OK to confirm delete\n Press Cancel to cancel") == true) {
			return true;
		}
		return false;
	}
</SCRIPT>


<style>
	html {
		font-size: 1rem;
	}

	@include media-breakpoint-up(sm) {
		html {
			font-size: 1.2rem;
		}
	}

	@include media-breakpoint-up(md) {
		html {
			font-size: 1.4rem;
		}
	}

	@include media-breakpoint-up(lg) {
		html {
			font-size: 1.6rem;
		}
	}

</style>

</head>

<body>
<?php require_once("navbar_index.php"); ?>

<script type="text/javascript"> 
	$(function() {     
		$('#mydate').calendarsPicker({calendar: $.calendars.instance('thai','th')}); 
		});
</script>

<?php
	if(ISSET($_GET['id'])){
		$id=$_GET['id'];
		$tblname="tag";
		$url="edittag.php";
		include_once("connect_db.php");
		$strSQL="SELECT * FROM tag WHERE id=$id";
		$result=$mysqli->query($strSQL);
		if($result->num_rows>0){			
			$row=$result->fetch_assoc();
			if(ISSET($_SESSION['status']) && $_SESSION['status']=="M"){
				if($_SESSION['section']!==$row['tagm_org']){
					echo '<br><br><center><span style="background-color:yellow">&nbsp;Access denied...! Not permissive to edit this record.</span></center>';
					exit();
				}
			}else{
				if($row['tago_no']=="" || empty($row['tago_no']) ){
					echo '<br><br><center><span style="background-color:yellow">&nbsp;Access denied...! Not permissive to edit this record.</span></center>';
					exit();
				}
			}
		}

		$cir1="[";
		$cir2="]";

		$strSQL="SELECT n.*, m.aks, m.name, m.breaker, m.location FROM tag n INNER JOIN equipment m ON (n.aks=m.aks) WHERE n.id=$id";
		$result=$mysqli->query($strSQL);
		$row=$result->fetch_assoc();
?>
<div class="container px-5 p-0">	
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 mx-auto">
			<div class="d-inline p-0 bg-primary text-white text-center">Edit Tag</div>
			<form name="form" method="post" action="edittag_act.php">
				<table class="table" id="data_grid" border=0>
					<thead>
						<tr bgcolor="#CCCC99" style="color:#000000" align="center">
							<td width="4%""><small>ID</small></td>
							<td width="8%"><small>M-Tag no.<br />O-Tag no.</small></td>
							<td width="15%"><small>AKS<br />Equipment</small></td>
							<td width="8%"><small>Breaker<br>Location</small></td>
							<td width="8%"><small>วันที่ขอ: M<br>วันที่ขอ: O</small></td>
							<td width="8%"><small>ผู้ขอแขวน-M<br>ผู้ขอแขวน-O</small></td>
							<td width="8%"><small>วันที่แขวน:M<br>วันที่แขวน:O</small></td>
							<td width="8%"><small>ผู้แขวน-M<br>ผู้แขวน-O</small></td>
							<td width="8%"><small>วันที่ปลด:M<br>วันที่ปลด:O</small></td>
							<td width="8%"><small>ผู้ปลด-M<br>ผู้ปลด-O</small></td>
							<td width="5%"><small>Key-M<br>Key-O</small></td>
							<td width="15%"><small>งานที่ขอทำ<br>Task</small></td>
						</tr>
					</thead>

					<tbody>	
						<tr>
							<td align="center" valign="center"><font size="1"><?php echo $row["id"]; ?></small><input type="hidden" name="id" value="<?php echo $row["id"] ?>"></td>

							<td align="center" valign="center"><font size="1"><?php echo $row["tagm_no"]; ?><br /><?php echo $row["tago_no"]; ?></font></td>

							<td align="center" valign="center"><font size="1"><input type="text" name="aks" id="aks" value="<?php echo $row["aks"] ?>" size="15"><br /><font color='green'><?php echo $row["name"]; ?></font></small></td>

							<td align="left" valign="center"><font size="1"><?php echo $row["breaker"]; ?>&nbsp;<?php echo $row["location"]; ?></td>
						
					<?php if($_SESSION["status"]=='M'){ ?>
							<td align="center" valign="center"><font size="1"><input type="text" name="tagm_date1" id="tagm_date1" value="<?php echo $row["tagm_date1"]; ?>"  size="10"><br />
							<?php echo (($row["tago_date1"]!="")?$row["tago_date1"]:"-");?></td>

							<td align="center" valign="center"><font size="1"><?php if($row['tagm_no']!=""){?><input type="text" name="tagm_h1" id="tagm_h1" value="<?php echo $row["tagm_h1"]; ?>"  size="10"><?php }?><br />
							<?php echo (($row["tago_h1"]!="")?$row["tago_h1"]:"-");?></td>

							<td align="center" valign="center"><font size="1"><?php echo (($row["tagmo_date2"]!="")?$row["tagmo_date2"]:"-");?></td>	
							<td align="center" valign="center"><font size="1"><?php echo $row["tagm_h2"]; ?><br /><?php echo $row["tago_h2"]; ?></td>	

							<td align="center" valign="center"><font size="1"><?php echo (($row["tagm_outdate"]!="")?$row["tagm_outdate"]:"-");?><br /><?php echo (($row["tago_outdate"]!="")?$row["tago_outdate"]:"-");?></td>	

							<td align="center" valign="center"><font size="1"><?php echo (($row["tagm_outby"]!="")?$row["tagm_outby"]:"-");?><br /><?php echo (($row["tago_outby"]!="")?$row["tago_outby"]:"-");?></td>	
						<?php } ?>

						<?php if($_SESSION["status"]=='O'){?>
							<td align="center" valign="center"><font size="1"><?php if($row['tagm_no']!=""){?><input type="text" name="tagm_date1" id="tagm_date1" value="<?php echo $row["tagm_date1"]; ?>" size="10"><?php }?>	<br />
							<?php if($row['tago_date1']!=""){?><input type="text" name="tago_date1" id="tago_date1" value="<?php echo $row["tago_date1"] ?>" size="10"><?php }?></td>
							
							<td align="center" valign="center"><font size="1"><?php if($row['tagm_no']!=""){?><input type="text" name="tagm_h1" id="tagm_h1" value="<?php echo $row["tagm_h1"]; ?>"  size="10"><?php }?>	<br />
							<?php if($row['tago_no']!=""){?><input type="text" name="tago_h1" id="tago_h1" value="<?php echo $row["tago_h1"] ?>" size="10"><?php }?></td>
							
							<td align="center" valign="center"><font size="1"><?php if($row['tago_h2']!=""){?><input type="text" name="tagmo_date2" id="tagmo_date2" value="<?php echo $row["tagmo_date2"]; ?>" size="10"><?php }?></td>

							<td align="center" valign="center"><font size="1"><?php if($row['tagm_no']!="" && $row['tagmo_date2']!=""){?><input type="text" name="tagm_h2" id="tagm_h2" value="<?php echo $row["tagm_h2"]; ?>" size="10"><?php }?><br /><?php if($row['tago_no']!="" && $row["tago_h2"]!=""){?><input type="text" name="tago_h2" id="tago_h2" value="<?php echo $row["tago_h2"] ?>" size="10"><?php }?></td>

							<td align="center" valign="center"><font size="1"><?php echo (($row["tagm_outdate"]!="")?$row["tagm_outdate"]:"-");?><br /><?php echo (($row["tago_outdate"]!="")?$row["tago_outdate"]:"-");?></td>	

							<td align="center" valign="center"><font size="1"><?php echo (($row["tagm_outby"]!="")?$row["tagm_outby"]:"-");?><br /><?php echo (($row["tago_outby"]!="")?$row["tago_outby"]:"-");?></td>	
						<?php }?>

						<?php if($row['tagmo_date2']!=""){?>
							<?php if($row['tagm_no']!=""){?>
								<td align="center" valign="center"><font size="1"><?php echo $row["keym_id"]; ?><font color='red'>&nbsp;<?=($row["keym_status"]!=''?$cir1.$row["keym_status"].$cir2:''); ?></font><br /><font size="1"><?php echo $row["keyo_id"]; ?><font color='red'>&nbsp;<?=($row["keyo_status"]!=''?$cir1.$row["keyo_status"].$cir2:''); ?></td>	
							<?php }else{?>
								<td align="center" valign="center"><font size="1">&nbsp;<br /><font size="1"><?php echo $row["keyo_id"]; ?><font color='red'>&nbsp;<?=($row["keyo_status"]!=''?$cir1.$row["keyo_status"].$cir2:''); ?></td>	
							<?php }?>
						<?php }else{?>
								<td align="center" valign="center"><font size="1"><?php echo $row["keym_id"]; ?><br /><font size="1"><?php echo $row["keyo_id"]; ?></td>	
						<?php }?>			

							<td align="left" valign="center"><font size="1"><input type="text" name="task" id="task" value="<?php echo $row["task"] ?>"  size="25"></td>
						</tr>

						<tr>
							<td colspan="12" align=center><input type="submit" name="submit" id="submit" value="Submit">
							<input type="reset" name="btnCancel" value="Cancel">&nbsp;<a href="delrecord.php?id=<?=$id?>&tb=<?=$tblname?>&url=<?=$url?>" onClick="return Conf(this)"><font size="1" color="red">DELETE</a></td>
						</tr>
					</tbody>
				</table>
			</form>
    	</div>
  	</div>
</div>
		<?php 
			}
		?>


</body>
</html>