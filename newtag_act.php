<?php /**By Anek suriwongyai 05-05-2562 */ 
    session_start();
    require_once("includes/function.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<script src="./js/jquery.min.js"></script>

<link rel="stylesheet" href="./css/bootstrap.min.css" media="all">
<link rel='stylesheet' type='text/css' href='./css/style.css'>
<title>LockOut TagOut (Tag Information)</title>
<link rel="stylesheet" type="text/css" href="dist/jquery.dataTables.min.css"/>	 
<script type="text/javascript" src="dist/jquery.dataTables.min.js"></script>

<style media="all" type="text/css">
	.round_image{
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		-webkit-box-shadow: #000 0 2px 10px;
		-moz-box-shadow: #000 0 2px 10px;
		box-shadow: #000 0 2px 10px;
	}
</style>

<style>
	@media (min-width: 500px) {
			.container{
				width: 800px;
                align-content: center;
			}
	}
</style>
 
</head>
<body>
<?php 
    require_once("navbar_index.php");
    if(isset($_POST['submit'])){
        $adduser=$_SESSION["EGATID"];
        $gdate=date("Y-m-d H:i:s");
        $clientip=$_SERVER["REMOTE_ADDR"];
        $unit=$_SESSION["unit"];
        $org=$_SESSION["section"];
        $url="index.php";
        $tblname='tag';
        include_once("connect_db.php");
        $status=$_SESSION["status"];
        $rec=$_POST['rec'];

        if($status!='O'){
            $sqlTag="SELECT tagm_no FROM $tblname WHERE tagm_org='$org' ORDER BY id DESC LIMIT 1";
            $result=$mysqli->query($sqlTag);
            if($result->num_rows>0){
                $row=$result->fetch_assoc();
                $tagno=$row['tagm_no'];
            }
            if ($tagno=='' || empty($tagno)){ 
                $tagno=$org.$unit.':'.'1';
            }else{
                $tagno=getTagno($tagno);
            }

            for($i=0;$i<=$rec-1;$i++){
                if($_POST['task'.$i] != "" and isset($_POST['checked'.$i])){
                    $tag_no=$tagno;
                    $aks=$_POST['aks'.$i];
                    $task=$_POST['task'.$i];
                    $keyid=$_POST['keyid'.$i];
                    $workorder=$_POST['workorder'.$i];
                    $hanger=$_POST["hanger"];
                    $tag_date1=thaiDate($_POST["mydate"]);
                    $insert = "INSERT INTO $tblname (aks, task, tagm_no, tagm_org, tagm_date1, tagm_h1, keym_id, workorder, tagm_addby, tagm_addtime, tagm_addip)";
                    $insert .=" VALUES ('".$aks."', '".$task."', '".$tag_no."', '".$org."', '".$tag_date1."', '".$hanger."', '".$keyid."', '".$workorder."', '".$adduser."', '".$gdate."', '".$clientip."')" ;
                    $mysqli->query("SET NAMES 'utf8'");
                    $result = $mysqli->query($insert);
                    $tagno=getTagno($tagno);                  
                }		
            }
            $affected_rows = $mysqli->affected_rows;
        }else{
            $sqlTag="SELECT tago_no FROM $tblname WHERE tago_org like '%".$org."%' and tagm_no IS NULL ORDER BY id DESC LIMIT 1";
            $result=$mysqli->query($sqlTag);
            if($result->num_rows>0){
                $row=$result->fetch_assoc();
                $tagno=$row['tago_no'];
            }

            if ($tagno=='' || empty($tagno)){ 
                $tagno=$org.$unit.':'.'1';
            }else{
                $tagno=getTagno($tagno);
            }

            for($i=0;$i<=$rec-1;$i++){
                if($_POST['task'.$i] != "" and isset($_POST['checked'.$i])){
                    $tag_no=$tagno;
                    $aks=$_POST['aks'.$i];
                    $task=$_POST['task'.$i];
                    $keyid=$_POST['keyid'.$i];
                    $workorder=$_POST['workorder'.$i];
                    $hanger=$_POST["hanger"];
                    $tag_date1=thaiDate($_POST["mydate"]);
                    $insert = "INSERT INTO $tblname (aks, task, tago_no, tago_org, tago_date1, tago_h1, keyo_id, workorder, tago_addby, tago_addtime, tago_addip)";
                    $insert .=" VALUES ('".$aks."', '".$task."', '".$tag_no."', '".$org."', '".$tag_date1."', '".$hanger."', '".$keyid."', '".$workorder."', '".$adduser."', '".$gdate."', '".$clientip."')" ;
                    $mysqli->query("SET NAMES 'utf8'");
                    $result = $mysqli->query($insert);
                    $tagno=getTagno($tagno);                  
                }		
            }
            $affected_rows = $mysqli->affected_rows;
        }
    }
?> 

<?php 

if ( @$affected_rows == 1 ) {
        $msg="Insert the new data successful...";
        echo '<center><br /><br /><table width="60%" border="0" cellspacing="0"><tr bgcolor="#eee" height="110"><td align="center">';
        echo '<br/><font face="MS Sans Serif"><font color="red" size="+1">'.$msg.'</font></font><br/>' ;
        echo '<img src="images/process.gif"/><br/>' ;	
        echo '<meta http-equiv="refresh" content="2; URL='.$url.'">' ;
        echo '</tr></td></table></center>';
    }else{
        $msg="Can not Insert data...!";
        echo '<center><br /><br /><table width="40%" border="0" cellspacing="0"><tr bgcolor="#eee" height="110"><td align="center">';
        echo '<br/><font face="MS Sans Serif"><font color="red" size="+1">'.$msg.'</font></font><br/>' ;
        echo '<img src="images/process.gif"/><br/>' ;	
        echo '<meta http-equiv="refresh" content="2; URL='.$url.'">' ;
        echo '</tr></td></table></center>';
    }
?>
</body>
</html>