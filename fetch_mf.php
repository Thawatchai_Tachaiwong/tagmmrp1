<?php
	session_start();
	$tbname1 = "tag";
	$tbname2 = "equipment";
	if($_SESSION['starttime'] == ''){
		$start_time= date('Y-m-d');
	}else{
		$start_time = $_SESSION['starttime'];
	}
	if($_SESSION['endtime'] == ''){
		$edate= date('Y-m-d');
	}else{
		$edate = $_SESSION['endtime'];
	}

	if($_SESSION['starttime'] == ''){		
		$strSql = "SELECT COUNT(n.id) FROM $tbname1 n INNER JOIN $tbname2 m ON n.aks = m.aks";
        $strSql .= " WHERE n.tago_h1 IS NOT NULL AND n.tago_h2 IS NULL";
	}else{
		$strSql = "SELECT COUNT(n.id) FROM $tbname1 n INNER JOIN $tbname2 m ON n.aks = m.aks";
		$strSql .= " WHERE n.tago_h1 IS NOT NULL AND n.tago_h2 IS NULL";
	}

	//continue only if $_POST is set and it is a Ajax request
	if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
		
		include("config.inc.php");
		//Get page number from Ajax POST
		if(isset($_POST["page"])){
			$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
			if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
		}else{
			$page_number = 1; //if there's no page number, set it to 1
		}
		
		$results = $mysqli->query($strSql);
		$get_total_rows = $results->fetch_row(); //hold total records in variable
		
		$total_pages = ceil($get_total_rows[0]/$item_per_page);
		
		$page_position = (($page_number-1) * $item_per_page);

		if($_SESSION['starttime'] == ''){
			$strSQL = "SELECT n.*, m.aks, m.name, m.breaker, m.location FROM $tbname1 n INNER JOIN $tbname2 m ON n.aks = m.aks";             
            $strSQL .= " WHERE n.tago_h1 IS NOT NULL AND n.tago_h2 IS NULL ORDER BY n.id DESC LIMIT $page_position, $item_per_page";
		}else{
            $strSQL = "SELECT n.*, m.aks, m.name, m.breaker, m.location FROM $tbname1 n INNER JOIN $tbname2 m ON n.aks = m.aks";             
			$strSQL .= " WHERE n.tago_h1 IS NOT NULL AND n.tago_h2 IS NULL ORDER BY n.id DESC LIMIT $page_position, $item_per_page";
		}


		echo '<br>\$strSql = '.$strSql;
		echo '<br>';
		echo '<br>\$strSQL = '.$strSQL;
		echo '<br>';

		$result = $mysqli->query($strSQL);
		if($result) {
			echo '<table width="100%" class="sample"><caption>MF-00-ASC-37-01-02</caption>';
            ?>

            <tr bgcolor="skyblue" align="center">
            <th rowspan="2" width='4%'><font size=1>ลำดับ</th>
            <th rowspan="2" width='12%'><font size=1>AKS<br>equipment</th>
            <th rowspan="2" width="7%"><font size=1>SWITCH<br>GEAR</th>
            <th rowspan="2" width='10%'><font size=1><font color='red'>MT-TAG NO.</font><br><font color='blue'>OP-TAG NO.</font></th>	
			<th rowspan="2" width='14%'><font size=1><font color='red'>MT:วันที่ขอแขวน-ผู้แขวน </font><br><font color='blue'>OP:วันที่ขอแขวน-ผู้แขวน </font></th>
            <th rowspan="2" width='6%' bgcolor='#FFFFCC'><font size=1>วันที่แขวนจริง</th>
            <th colspan="2" width='17%' bgcolor='#FFFFCC'><font size=1>ผู้ยืนยันการแขวน</th>
            <th rowspan="2" width='6%' bgcolor='#FFFFCC'><font size=1>วันที่ปลดจริง</th>
            <th colspan="2" width='17%' bgcolor='#FFFFCC'><font size=1>ผู้ยืนยันการปลด</th>
            </tr>
            <tr bgcolor="skyblue" align="center">
            <th width='7%' bgcolor='#FFFFCC'><font size=1>เลขประจำตัว</th>
            <th width='10%' bgcolor='#FFFFCC'><font size=1>ลายเซ็น</th>
            <th width='7%' bgcolor='#FFFFCC'><font size=1>เลขประจำตัว</th>
            <th width='10%' bgcolor='#FFFFCC'><font size=1>ลายเซ็น</th>		
            </tr>
            <?php 
			while($row = $result->fetch_assoc() ) {
            ?>  
                </tr>
                    <td align=center><font size=1><?php echo $row["id"]; ; ?></font></td>
                    <td align=center><font size=1><?php echo $row["aks"]; ?>&nbsp;<br>&nbsp;<?php echo $row["name"]; ?></font></td>
                    <td align=center><font size=1><?php echo $row["breaker"]; ?></td>
                    <td align=center><font size=1><font color='red'><?=(($row["tagm_no"]!="")?$row['tagm_no']:"--")?><br><font color='blue'><?=(($row["tago_no"]!="")?$row["tago_no"]:"--")?></td>
                    <td align=center><font size=1><font color='red'><small><?=(($row["tagm_date1"]!="")?$row["tagm_date1"]:"--")?>&nbsp;-&nbsp;<?=(($row["tagm_h1"]!="")?$row["tagm_h1"]:"--")?><br><font color='blue'><?=(($row["tago_date1"]!="")?$row["tago_date1"]:$row["tagm_date1"])?>&nbsp;-&nbsp;<?=(($row["tago_h1"]!="")?$row["tago_h1"]:"--")?></td>				
                    <td align=center>&nbsp;</td>
                    <td align=center>&nbsp;</td>
                    <td align=center>&nbsp;</td>
                    <td align=center>&nbsp;</td>
                    <td align=center>&nbsp;</td>
                    <td align=center>&nbsp;</td>
                </tr>

            <?php 
            }
			echo '</table><br />';
		}

        echo '<div align="center">';
		echo paginate_function($item_per_page, $page_number, $get_total_rows[0], $total_pages);
		echo '</div>';
		//echo "<br />strSQLi=".$strSQL;
		exit;
	}
	################ pagination function #########################################
	function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
	{
		$pagination = '';
		if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
			$pagination .= '<ul class="pagination">';
			
			$right_links    = $current_page + 3; 
			$previous       = $current_page - 3; //previous link 
			$next           = $current_page + 1; //next link
			$first_link     = true; //boolean var to decide our first link
			
			if($current_page > 1){
				$previous_link = ($previous==0)? 1: $previous;
				$pagination .= '<li class="first"><a href="#" data-page="1" title="First">&laquo;</a></li>'; //first link
				$pagination .= '<li><a href="#" data-page="'.$previous_link.'" title="Previous">&lt;</a></li>'; //previous link
					for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
						if($i > 0){
							$pagination .= '<li><a href="#" data-page="'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
						}
					}   
				$first_link = false; //set first link to false
			}
			
			if($first_link){ //if current active page is first link
				$pagination .= '<li class="first active">'.$current_page.'</li>';
			}elseif($current_page == $total_pages){ //if it's the last active link
				$pagination .= '<li class="last active">'.$current_page.'</li>';
			}else{ //regular current link
				$pagination .= '<li class="active">'.$current_page.'</li>';
			}
					
			for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
				if($i<=$total_pages){
					$pagination .= '<li><a href="#" data-page="'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
				}
			}
			if($current_page < $total_pages){ 
					$next_link = ($i > $total_pages) ? $total_pages : $i;
					$pagination .= '<li><a href="#" data-page="'.$next_link.'" title="Next">&gt;</a></li>'; //next link
					$pagination .= '<li class="last"><a href="#" data-page="'.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
			}
			
			$pagination .= '</ul>'; 
		}
		return $pagination; //return pagination links
	}
?>