<?php   //09-06-2019 by anek suriwongyai
session_start();
include('connexion.php');
 
if($_POST) {

	$page = $_POST['page']; // Current page number
	$per_page = $_POST['per_page']; // Articles per page
	if ($page != 1) $start = ($page-1) * $per_page;
    else $start=0;
    
    $status = $_SESSION['status'];
    $sec_id = $_SESSION['section'];

    if($status == "O"){
        if(isset($_SESSION['strSearch']) && $_SESSION['strSearch']!=""){
            $strSearch=$_SESSION['strSearch'];

            $strSQL = 'SELECT n.*, m.aks, m.name, m.breaker, m.location FROM tag n INNER JOIN equipment m ON n.aks = m.aks';             
            $strSQL .= ' WHERE '.$strSearch.' ORDER BY n.id DESC LIMIT '.$start.', '.$per_page.'';
            $select = $bdd->query($strSQL);
            $select->setFetchMode(PDO::FETCH_OBJ);
            
            $strSQL = 'SELECT COUNT(n.id) FROM tag n INNER JOIN equipment m ON n.aks = m.aks';
            $strSQL .= ' WHERE '.$strSearch;
            $numArticles = $bdd->query($strSQL)->fetch(PDO::FETCH_NUM);
        }else{
            $strSQL = 'SELECT n.*, m.aks, m.name, m.breaker, m.location FROM tag n INNER JOIN equipment m ON n.aks = m.aks';             
            $strSQL .= ' ORDER BY n.id DESC LIMIT '.$start.', '.$per_page.'';
            $select = $bdd->query($strSQL);
            $select->setFetchMode(PDO::FETCH_OBJ);
            
            $strSQL = 'SELECT COUNT(n.id) FROM tag n INNER JOIN equipment m ON n.aks = m.aks';
            // $strSQL .= ' WHERE n.tago_h1 IS NOT NULL AND n.tago_h2 IS NULL';
            $numArticles = $bdd->query($strSQL)->fetch(PDO::FETCH_NUM); 
        }
    }

    if($status == "M"){
        if(isset($_SESSION['strSearch']) && $_SESSION['strSearch']!=""){
            $strSearch=$_SESSION['strSearch'];

            $strSQL = 'SELECT n.*, m.aks, m.name, m.breaker, m.location FROM tag n INNER JOIN equipment m ON n.aks = m.aks';             
            $strSQL .= ' WHERE '.$strSearch.' AND n.tagm_org ="'.$sec_id.'" ORDER BY n.id DESC LIMIT '.$start.', '.$per_page.'';
            $select = $bdd->query($strSQL);
            $select->setFetchMode(PDO::FETCH_OBJ);
            
            $strSQL = 'SELECT COUNT(n.id) FROM tag n INNER JOIN equipment m ON n.aks = m.aks';
            $strSQL .= ' WHERE '.$strSearch.' AND n.tagm_org ="'.$sec_id.'"';
            $numArticles = $bdd->query($strSQL)->fetch(PDO::FETCH_NUM);
        }else{
            $strSQL = 'SELECT n.*, m.aks, m.name, m.breaker, m.location FROM tag n INNER JOIN equipment m ON n.aks = m.aks';             
            $strSQL .= ' WHERE n.tagm_org ="'.$sec_id.'" ORDER BY n.id DESC LIMIT '.$start.', '.$per_page.'';
            $select = $bdd->query($strSQL);
            $select->setFetchMode(PDO::FETCH_OBJ);
            
            $strSQL = 'SELECT COUNT(n.id) FROM tag n INNER JOIN equipment m ON n.aks = m.aks';
            $strSQL .= ' WHERE n.tagm_org ="'.$sec_id.'"';
            $numArticles = $bdd->query($strSQL)->fetch(PDO::FETCH_NUM); 
        }
    }

	$numPage = ceil($numArticles[0] / $per_page); // Total number of page
	
    $articleList = '<table class="table table-hover px-0 p-0 mt-0" border=0">';

	// $articleList = '<table border=0 width=100%>';
	while( $result = $select->fetch() ) {
        $articleList .= '<tr><td align=center width=4%><font size=1>'.$result->id.'</font></td>';
        $articleList .= '<td align=center width=13%><font size=1>'.$result->aks.'&nbsp;<br>&nbsp;'.$result->name.'</font></td>';
        $articleList .= '<td align=center width=9%><font size=1>'.$result->breaker.'</td>';
        $articleList .= '<td align=center width=12%><font size=1><font color="red">'.$result->tagm_no.'<br><font color="blue">'.$result->tago_no.'</td>';
        $articleList .= '<td align=center width=13%><font size=1><font color="red"><small>'.$result->tagm_date1.'&nbsp;-&nbsp;'.$result->tagm_h1.'<br><font color="blue">'.$result->tago_date1.'&nbsp;-&nbsp;'.$result->tago_h1.'</td>';				
        $articleList .= '<td align=center width=7%>&nbsp;</td>';
        $articleList .= '<td align=center width=7%>&nbsp;</td>';
        $articleList .= '<td align=center width=11%>&nbsp;</td>';
        $articleList .= '<td align=center width=7%>&nbsp;</td>';
        $articleList .= '<td align=center width=7%>&nbsp;</td>';
        $articleList .= '<td align=center width=11%>&nbsp;</td></tr>';
    }
    $articleList .= '<tr><td align=right colspan=11>MF-00-ASC-37-01-02</td></tr>';
    $articleList .='</table>';
    
    $dataBack = array('numPage' => $numPage, 'articleList' => $articleList);
	$dataBack = json_encode($dataBack);
	
    echo $dataBack;
}
?>