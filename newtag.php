<?php   /**By Anek suriwongyai 03-05-2562 */
	session_start();
	ini_set('display_errors', 1);
	error_reporting(~0);
	require_once("getowner.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<script src="./js/jquery.min.js"></script>

<link rel="stylesheet" href="./css/bootstrap.min.css" media="all">
<link rel='stylesheet' type='text/css' href='./css/style.css'>
<title>New tag</title>
<!-- <link rel="stylesheet" type="text/css" href="dist/jquery.dataTables.min.css"/>	  -->
<!-- <script type="text/javascript" src="dist/jquery.dataTables.min.js"></script> -->

<style media="all" type="text/css">
	.round_image{
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		-webkit-box-shadow: #000 0 2px 10px;
		-moz-box-shadow: #000 0 2px 10px;
		box-shadow: #000 0 2px 10px;
	}
</style>
<style>
	@media (min-width: 1500px) {
			.container{
				width: 1400px;
			}
	}
</style>

<!--*********Start calendar************-->
<link type="text/css" href="jquery/flora.calendars.picker.css" rel="stylesheet"/> 
<script type="text/javascript" src="jquery/jquery.min.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.min.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.plus.min.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.picker.min.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.thai.min.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.thai-th.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.picker-th.js"></script> 

<script language="javascript">
	function checkdate_click(){
			if(document.getElementById('checkdate').checked==true){
					document.getElementById('submit').disabled = false;
			}else{
					document.getElementById('submit').disabled = true;
			}	
	}
</script>

</head>

<body>
<?php require_once("navbar_index.php"); ?>

<script type="text/javascript"> 
$(function() {     
	$('#mydate').calendarsPicker({calendar: $.calendars.instance('thai','th')}); 
	});
</script>

<div class="container px-5 p-0">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 mx-auto">
				<table cellpadding="1" cellspacing="0" width="60%" border="0">  
					<form name="frmSearch" method="post" action="<?php echo $_SERVER['SCRIPT_NAME'];?>"> 
						<tr><td><font size='-1'>&nbsp;<input type="text" name="strSearch" id="strSearch" value=""> 
						<input type="submit" name="search" id="search" value="Search">&nbsp;&nbsp;<input type="submit" name="reset" id="reset" value="Reset"></td></tr>
					</form> 
				</table>
		</div>
	</div>
</div>

<?php
	$tbname="equipment";

	if (ISSET($_POST["search"])){
		$strSearch=$_POST["strSearch"];
		if (empty($strSearch)){
			$strSQL = "SELECT count('id') FROM $tbname"; 
		}else{
			$strSQL = "SELECT count('id') FROM $tbname WHERE (aks LIKE '%$strSearch%') or (name LIKE '%$strSearch%') or (breaker LIKE '%$strSearch%')";
			$no_of_records_per_page = 80;
		}
	}else{
		$strSQL = "SELECT count('id') FROM $tbname";
		$no_of_records_per_page = 8;
	}

	if (isset($_GET['pageno'])) {
			$pageno = $_GET['pageno'];
	} else {
			$pageno = 1;
	}
	if($no_of_records_per_page==""){
		$no_of_records_per_page = 8;
	}
	$offset = ($pageno-1) * $no_of_records_per_page;

	include_once("connect_db.php");
	
	$result = $mysqli->query($strSQL);
	$row = $result->fetch_row();
	$total_rows = $row[0];
	$total_pages = ceil($total_rows / $no_of_records_per_page);

	if (empty($strSearch)){	
		$sql = "SELECT * FROM $tbname ORDER BY id ASC LIMIT $offset, $no_of_records_per_page";
	}else{
		$sql = "SELECT * FROM $tbname WHERE (aks LIKE '%$strSearch%') or (name LIKE '%$strSearch%') or (breaker LIKE '%$strSearch%')  ORDER BY id ASC LIMIT $offset, $no_of_records_per_page";
	}
	$mysqli->query("SET NAMES 'utf8'");
	$result = $mysqli->query($sql);
	$rows = $result->fetch_row();
	$total_row = $rows[0];
	$i=0;
	
?>

<div class="container px-5 p-0">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 mx-auto">
			<form name="form" method="post" action="newtag_act.php">
				<table class="table" id="data_grid" border=0>
			<!--<table id="data_grid" class="display" cellspacing="0">-->
					<thead>
						<tr>
							<th>ITEM</th>
							<th>AKS.</th>
							<th>Equipment</th>
							<th>Breaker or Location</th>
							<th>Key lock ID</th>
							<th>Task (งานที่ทำ)</th>
							<th>Work Order Ref.</th>
							<th>Check (เลือก)</th>
						</tr>
					</thead>

				<?php 
					$i=0;
					while($row = $result->fetch_assoc()) {
						echo '<tr><td align="center"><small>'.$row['id'].'</small></td>';
						echo '<td><small>'.$row['aks'].'</small></td>';
						echo '<td><small>'.$row['name'].'</small></td>';
						$switgear=$row['breaker']!=""?$row['breaker']:$row['location'];
						echo '<td><small><input type="hidden" name="aks'.$i.'" id="aks'.$i.'" value="'.$row['aks'].'">'.$switgear.'</small></td>';
						echo '<td align="center"><small><input type="text" name="keyid'.$i.'" id="keyid'.$i.'" size="5"></small></td>';
						echo '<td align="center"><small><input type="text" name="task'.$i.'" id="task'.$i.'" size="20"></small></td>';
						echo '<td align="center"><small><input type="text" name="workorder'.$i.'" id="workorder'.$i.'" size="10"></small></td>';
						echo '<td align="center"><small><input type="checkbox" name="checked'.$i.'" id="checked'.$i.'" value="'.$i.'"></small></td></tr>';
						$i++;
					}
				?>
					<tr><td colspan="8">
						<ul class="pagination">
							<li><a href="?pageno=1">First</a></li>
							<li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
								<a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">Prev</a>
							</li>
							<li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
								<a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">Next</a>
							</li>
							<li><a href="?pageno=<?php echo $total_pages; ?>&sqlc=<?php echo $strSQL; ?>">Last</a></li>
							<li></li>
							<?php $i=$i+1; ?>
						</ul>
					</td></tr>
					<tr><td colspan="4" align="right"><input type="checkbox" name="checkdate" id="checkdate" onclick="checkdate_click()" />
					&nbsp;วันที่ขอแขวน : <input type="text"  name="mydate" id="mydate" size="10"/></td>
					
					<?php 
						if(ISSET($_SESSION["status"])){
							$section=$_SESSION["section"];
							$strSQLi = "SELECT egat_id, name FROM hanger WHERE section_id='$section' ORDER BY egat_id ASC;";
							$mysqli->query("SET NAMES 'utf8'");
							$results = $mysqli->query($strSQLi);
							echo '<td colspan="4" align="left">ผู้แขวน : ';
							echo '<select name="hanger" id="hanger">';
							while($drop = $results->fetch_assoc()) {
								echo '<option value="'.$drop["egat_id"].'">'.$drop["egat_id"].' : '.$drop["name"].'</option>';
							}
							echo '</select></td></tr>';
						}else{
							echo '<tr><td colspan="8">Not hanger data</td></tr>';
						}
					?>
					<tr height='30'><td align='center' colspan='8'><input type="hidden" name="rec" value="<?php echo $i;?>"><input type="submit" name="submit" id="submit" value="SUBMIT" disabled="disabled"></td></tr>

				</table>
			</form>
    	</div>
  	</div>
</div>
</body>
</html>