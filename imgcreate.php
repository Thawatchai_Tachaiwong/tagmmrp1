<?php
    session_start() ;
    header("Content-type: image/png");
    include_once("connect_db.php");

    if(ISSET($_GET['id'])){
        $id=$_GET["id"];
    }else{
        echo 'No data select';
    }
    //$id=$_GET["id"];
	$id=585;
	$tbname1='tag';
	$tbname2='equipment';
	$status=$_SESSION["status"];
	$sectionid=$_SESSION["section"];
	$printtag="Y";
	$gdate=date("Y-m-d H:i:s");
	$clientip=$_SERVER["REMOTE_ADDR"];
	if($clientip=="::1"){
		$clientip="127.0.0.1";
	}

	$strSQL = "SELECT n.*, m.aks, m.name, m.breaker, m.location FROM $tbname1 n INNER JOIN $tbname2 m ON (n.aks=m.aks)";
	$strSQL .= " WHERE n.id= $id";

	$mysqli->query("SET NAMES 'utf8'");
	$result = $mysqli->query($strSQL);
	$affected = $result->num_rows;
    $row = $result->fetch_assoc();
    $recid = $row["id"];
    // $aks = $row["aks"];
    $desp = $row["name"];
    $task = $row["task"];
    $bkr = $row["breaker"].' ('.$row["aks"].')';
    $otagid = $row["tago_no"];
    $owner = $row["tago_org"]; //แผนกเดินเครื่อง
    if($row["tailby"] == ""){   
        $taguser = $row["tago_addby"]; //ผู้รับผิดชอบ
    }else{
        $taguser = $row["tailby"];
    }
    $taghang = $row["tago_h1"]; //ผู้จะแขวน
    $hangrequest = $row["tago_date1"]; //วันที่ขอแขวน

    $sqlOrg = "SELECT name FROM org WHERE sec_id='$owner'";
    $mysqli->query("SET NAMES 'utf8'");
    $result = $mysqli->query($sqlOrg);
    $affected = $result->num_rows;
    $row = $result->fetch_assoc();
    $newowner = $row["name"];
    $sqlHanger = "SELECT name FROM hanger WHERE egat_id='$taghang'";
    $mysqli->query("SET NAMES 'utf8'");
    $result = $mysqli->query($sqlHanger);
    $affected = $result->num_rows;
    $row = $result->fetch_assoc();
    $hangman12 = $row["name"];

    $sqlUser = "SELECT name, phone FROM user WHERE egat_id='$taguser'";
    $mysqli->query("SET NAMES 'utf8'");
    $result = $mysqli->query($sqlUser);
    $affected = $result->num_rows;
    $row = $result->fetch_assoc();
    $user12 = $row["name"];
    $phone = $row["phone"];

    // $file='image/tag55.jpg';
    $txt=$otagid;
    $txt1=$desp;
    $txt2=$bkr;
    $txt3 = "ห้าม ON Breaker";
    // $txt3 = tis620_to_utf8($task1);
    //$txt3 = $task1;
    $txt4 = $task;
    $txt5 = $newowner;
    $txt6 = $phone;
    $txt7 = $user12;
    $txt7 = "(".$taguser.") ".$txt7;
    $txt8 = $hangman12;
    $txt8 = "(".$taghang.") ".$txt8;
    list($Y,$m,$d) = explode('-', $hangrequest);

    $txt9 = $d.'           '.$m.'          '.$Y;


    $font = dirname(__FILE__) . './tahomabd.ttf';
    // $font = dirname(__FILE__) . './fonts/arialbd.ttf';
    // $font = 'tahoma.ttf'; 
    // $font1 = 'tahomabd.ttf'; 
    $font_size = 12;
    $font_size1 = 10;
    $font_size2 = 9;

    $file='image/tag11.jpg';
    $im = imagecreatefromjpeg($file);
    $color = imagecolorallocate($im, 0, 0, 0);

    // White background and blue text
    // $bg = imagecolorallocate($im, 255, 255, 255);
    //$textcolor = imagecolorallocate($im, 0, 0, 255);
    // $textcolor = imagecolorallocate($im, 0, 0, 0);

    // $txt01 = "Tag Information";
    // Write the string at the top left
    // imagestring($im, 5, 0, 0, $txt1, $textcolor);


    $i=0;
    $pxX = imagesx($im)/6+40; //Tag number
    $pxY = imagesy($im)/4-90+$i;

    $pxX1 = imagesx($im)/6+80; //Name
    $pxY1 = imagesy($im)/2-48+$i;

    $pxX2 = imagesx($im)/6+80; 
    $pxY2 = imagesy($im)/2-5-$i;

    $pxX3 = imagesx($im)/4+70; //breaker
    $pxY3 = imagesy($im)/2+34-$i;

    $pxX4 = imagesx($im)/6+10; //task
    $pxY4 = imagesy($im)/2+78-$i;

    $pxX5 = imagesx($im)/6+100; //section
    $pxY5 = imagesy($im)/2+122-$i;

    $pxX6 = imagesx($im)/2+140; //phone
    $pxY6 = imagesy($im)/2+122-$i;
    
    $pxX7 = imagesx($im)/6+130; //user operator
    $pxY7 = imagesy($im)/2+217-$i;
    
    $pxX8 = imagesx($im)/6+130; //Tag Hanger
    $pxY8 = imagesy($im)/2+306+$i;
    
    $pxX9 = imagesx($im)/6+70; //Tag Hanger
    $pxY9 = imagesy($im)/2+390+$i;
    
    //$pxY8 = imagesy($im)/2-84+$i;	
    //$pxY8 = imagesy($im)/2-78+$i;

    //imagestring ( resource $image , int $font , int $x , int $y , string $string , int $color ) : bool
    /*
    imagestring($im, 5, $pxX, $pxY, $txt, $textcolor);
    imagestring($im, 5, $pxX1, $pxY1, $txt1, $textcolor);
    imagestring($im, 5, $pxX1, $pxY2, $txt2, $textcolor);
    imagestring($im, $font, $pxX1, $pxY3, $txt3, $textcolor);
    */
    imagettftext($im, $font_size, 0, $pxX, $pxY, $color, $font, $txt);
    imagettftext($im, $font_size, 0, $pxX1, $pxY1, $color, $font, $txt1); 
    imagettftext($im, $font_size, 0, $pxX2, $pxY2, $color, $font, $txt2); 
    imagettftext($im, $font_size, 0, $pxX3, $pxY3, $color, $font, $txt3); 
    imagettftext($im, $font_size, 0, $pxX4, $pxY4, $color, $font, $txt4); 
    imagettftext($im, $font_size, 0, $pxX5, $pxY5, $color, $font, $txt5); 
    imagettftext($im, $font_size, 0, $pxX6, $pxY6, $color, $font, $txt6); 
    imagettftext($im, $font_size, 0, $pxX7, $pxY7, $color, $font, $txt7); 
    imagettftext($im, $font_size, 0, $pxX8, $pxY8, $color, $font, $txt8); 
    imagettftext($im, $font_size, 0, $pxX9, $pxY9, $color, $font, $txt9); 

    ImagePng($im);
    ImageDestroy ($im);
    // imageJpeg($im,null,100);
    // imagedestroy($im);

    /*
    imagettftext($im, 5, 0, $pxX1, $pxY1, $color, $font, $txt1); 
    imagettftext($im, 5, 0, $pxX1, $pxY2, $color, $font, $txt2); 
    imagettftext($im, 5, 0, $pxX1, $pxY3, $color, $font, $txt3); 
    imagettftext($im, $font_size1, 0, $pxX4, $pxY4, $color, $font, $txt4); 
    imagettftext($im, $font_size, 0, $pxX5, $pxY4, $color, $font, $phone); 
    imagettftext($im, $font_size1, 0, $pxX6, $pxY6, $color, $font, $txt7);
    imagettftext($im, $font_size1, 0, $pxX6, $pxY7, $color, $font, $txt8);
    imagettftext($im, $font_size1, 0, $pxX8, $pxY8, $color, $font, $hangday);
    */

    function tis620_to_utf8($text) {
        $utf8 = "";
        for ($i = 0; $i < strlen($text); $i++) {
            $a = substr($text, $i, 1);
            $val = ord($a);
        
            if ($val < 0x80) {
            $utf8 .= $a;
            } elseif ((0xA1 <= $val && $val < 0xDA) || (0xDF <= $val && $val <= 0xFB)) {
            $unicode = 0x0E00+$val-0xA0;
            $utf8 .= chr(0xE0 | ($unicode >> 12));
            $utf8 .= chr(0x80 | (($unicode >> 6) & 0x3F));
            $utf8 .= chr(0x80 | ($unicode & 0x3F));
            }
        }
        return $utf8;
    }
?>
