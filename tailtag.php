<?php   /**By Anek suriwongyai 11-05-2562 */
	session_start();
	require_once("getowner.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<script src="./js/jquery.min.js"></script>

<link rel="stylesheet" href="./css/bootstrap.min.css" media="all">
<link rel='stylesheet' type='text/css' href='./css/style.css'>
<title>Tail tag</title>
<link rel="stylesheet" type="text/css" href="dist/jquery.dataTables.min.css"/>	 
<script type="text/javascript" src="dist/jquery.dataTables.min.js"></script>

<style media="all" type="text/css">
	.round_image{
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		-webkit-box-shadow: #000 0 2px 10px;
		-moz-box-shadow: #000 0 2px 10px;
		box-shadow: #000 0 2px 10px;
	}
</style>
<style>
	@media (min-width: 1500px) {
			.container{
				width: 1600px;
			}
	}
</style>

</head>

<body>
<?php require_once("navbar_index.php"); ?>

<div class="container px-5 p-0">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 mx-auto">
				<table cellpadding="1" cellspacing="0" width="60%" border="0">  
					<form name="frmSearch" method="post" action="<?php echo $_SERVER['SCRIPT_NAME'];?>"> 
						<tr><td><font size='-1'>&nbsp;<input type="text" name="strSearch" id="strSearch" value=""> 
						<input type="submit" name="search" id="search" value="Search">&nbsp;&nbsp;<input type="submit" name="reset" id="reset" value="Reset"></td></tr>
					</form> 
				</table>
		</div>
	</div>
</div>

<?php
    if($_SESSION["status"]!="O"){
        echo '<br><br><center><span style="background-color:yellow">&nbsp;Access denied...! Not permissive this feature.</span></center>';
        exit();
    }else{
        $status=$_SESSION["status"];

        if (ISSET($_POST["search"])){
            $strSearch=$_POST["strSearch"];
            if (empty($strSearch)){
                $no_of_records_per_page = 8;
            }else{
                $no_of_records_per_page = 80;
            }
        }else{
            $no_of_records_per_page = 8;
        }

        if (isset($_GET['pageno'])) {
            $pageno = $_GET['pageno'];
        } else {
            $pageno = 1;
        }

        if($no_of_records_per_page==""){
            $no_of_records_per_page = 8;
        }

        $offset = ($pageno-1) * $no_of_records_per_page;

        $tbname1 = "tag";
        $tbname2 = "equipment";

        if (!empty($strSearch)){	
            $strSQL = "SELECT n.*, m.aks, m.name, m.breaker, m.location FROM $tbname1 n INNER JOIN $tbname2 m ON (n.aks=m.aks)";
            $strSQL .= " WHERE (n.tagm_no IS NOT NULL AND n.tago_no IS NULL)";
            $strSQL .= " AND ((m.aks LIKE '%$strSearch%') OR (m.name LIKE '%$strSearch%') or (m.breaker LIKE '%$strSearch%') OR (n.tagm_org LIKE '%$strSearch%'))";
        }else{
            $strSQL = "SELECT n.*, m.aks, m.name, m.breaker, m.location FROM $tbname1 n INNER JOIN $tbname2 m ON (n.aks=m.aks)";
            $strSQL .= " WHERE (n.tagm_no IS NOT NULL AND n.tago_no IS NULL)";
        }

        include_once("connect_db.php");
        $result = $mysqli->query($strSQL);
        $total_rows = $result->num_rows;
        $total_pages = ceil($total_rows / $no_of_records_per_page);

        if (!empty($strSearch)){
            $strSQL = "SELECT n.*, m.aks, m.name, m.breaker, m.location FROM $tbname1 n INNER JOIN $tbname2 m ON (n.aks=m.aks)";
            $strSQL .= " WHERE (n.tagm_no IS NOT NULL AND n.tago_no IS NULL)";
            $strSQL .= " AND ((m.aks LIKE '%$strSearch%') OR (m.name LIKE '%$strSearch%') or (m.breaker LIKE '%$strSearch%') OR (n.tagm_org LIKE '%$strSearch%'))";
            $strSQL .= " ORDER BY n.id DESC LIMIT $offset, $no_of_records_per_page";
        }else{
            $strSQL = "SELECT n.*, m.aks, m.name, m.breaker, m.location FROM $tbname1 n INNER JOIN $tbname2 m ON (n.aks=m.aks)";
            $strSQL .= " WHERE (n.tagm_no IS NOT NULL AND n.tago_no IS NULL)";
            $strSQL .= " ORDER BY n.id DESC LIMIT $offset, $no_of_records_per_page";
        }
        $mysqli->query("SET NAMES 'utf8'");
        $result = $mysqli->query($strSQL );
        $affected = $result->num_rows;
        $i=0;
    }
?>

<div class="container px-5 p-0">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 mx-auto">
			<form name="form" method="post" action="tailtag_act.php">
				<table class="table" id="data_grid" border=0>
					<thead>
						<tr>
							<th>ITEM</th>
							<th>Tag : M No.</th>
							<th>AKS. Equipment</th>
							<th>Breaker or Location</th>
							<th>วันที่ขอแขวน</th>
							<th>Task (งานที่ทำ)</th>
							<th>Tag : OP No.</th>
							<th>Check (เลือก)</th>
						</tr>
					</thead>

                <?php
                    if($affected>0){ 
                        $j=0;
                        while($row = $result->fetch_assoc()) {
                            echo '<tr><td align="center"><small><input type="hidden" name="tagm_no'.$j.'" id="tagm_no'.$j.'" value="'.$row['tagm_no'].'">'.$row['id'].'</small></td>';
                            echo '<td><small>'.$row['tagm_no'].'</small></td>';
                            echo '<td><small>'.$row['aks'].' : '.$row['name'].'</small></td>';
                            $switgear=$row['breaker']!=""?$row['breaker']:$row['location'];
                            echo '<td><small>'.$switgear.'</small></td>';
                            
                            echo '<td align="center"><small><input type="hidden" name="tagm_date'.$j.'" id="tagm_date'.$j.'" value="'.$row['tagm_date1'].'">'.$row['tagm_date1'].'</small></td>';
                            echo '<td><small>'.$row['task'].'</small></td>';
                            echo '<td align="center"><small><input type="text" name="tago_no'.$j.'" id="tago_no'.$j.'" size="20"></small></td>';
                    ?>

                            <td align="center"><small><input name="checked<?php echo $j;?>" type="checkbox" id="checked<?php echo $j;?>" value="<?php echo $j;?>" onclick="javascript:tago_no<?php echo $j;?>.value=tagm_no<?php echo $j;?>.value+'-<?php echo $status;?>';"></small>
                            </td></tr>

			    <?php
						    $j++ ;
			            }
			    ?>
					    <tr><td colspan="8">
                            <ul class="pagination">
                                <li><a href="?pageno=1">First</a></li>
                                <li class="<?php if($pageno <= 1){ echo 'disabled'; } ?>">
                                        <a href="<?php if($pageno <= 1){ echo '#'; } else { echo "?pageno=".($pageno - 1); } ?>">Prev</a>
                                </li>
                                <li class="<?php if($pageno >= $total_pages){ echo 'disabled'; } ?>">
                                        <a href="<?php if($pageno >= $total_pages){ echo '#'; } else { echo "?pageno=".($pageno + 1); } ?>">Next</a>
                                </li>
                                <li><a href="?pageno=<?php echo $total_pages; ?>&sqlc=<?php echo $strSQL; ?>">Last</a></li>
                                <li></li>
                                <?php $i=$i+1; ?>
                            </ul>
					    </td></tr>

					    <tr>
					
					    <?php 
                            $strSQLi = "SELECT egat_id, name FROM hanger WHERE status='$status' ORDER BY egat_id ASC;";
                            $mysqli->query("SET NAMES 'utf8'");
                            $results = $mysqli->query($strSQLi);
                            echo '<td colspan="8" align="left">ผู้แขวน : ';
                            echo '<select name="hanger" id="hanger">';
                            while($drop = $results->fetch_assoc()) {
                                echo '<option value="'.$drop["egat_id"].'">'.$drop["egat_id"].' : '.$drop["name"].'</option>';
                            }
                            echo '</select></td>';
					    ?>
                        </tr>
					    <tr height='30'>
                            <td align='center' colspan='8'><input type="hidden" name="rec" value="<?php echo $j;?>"><input type="submit" name="submit" id="submit" value="SUBMIT""></td>
                        </tr>
                    <?php } ?>
				</table>
			</form>
    	</div>
  	</div>
</div>
</body>
</html>