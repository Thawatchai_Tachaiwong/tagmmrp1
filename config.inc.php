<?php
	$db_username 		= 'root';
	$db_password 		= '';
	$db_name 			= 'tagmmrp1';
	$db_host 			= 'localhost';
	$item_per_page 		= 15;

	$mysqli = new mysqli($db_host, $db_username, $db_password, $db_name);
	if ($mysqli->connect_error) {
		die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
	}
?>