<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<script src="./js/jquery.min.js"></script>

<link rel="stylesheet" href="./css/bootstrap.min.css" media="all">
<link rel='stylesheet' type='text/css' href='./css/style.css'>
<title>Tag Detail</title>
<link rel="stylesheet" type="text/css" href="dist/jquery.dataTables.min.css"/>	 
<script type="text/javascript" src="dist/jquery.dataTables.min.js"></script>

<style media="all" type="text/css">
	.round_image{
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		-webkit-box-shadow: #000 0 2px 10px;
		-moz-box-shadow: #000 0 2px 10px;
		box-shadow: #000 0 2px 10px;
	}
</style>
<style>
	@media (min-width: 1500px) {
			.container{
				width: 1600px;
			}
	}
</style>

<style type="text/css">
	body {
		font-family: ms Sans Serif;
	}

	op {
		font-size: small;
		color: blue;
		}
	mm {
		font-size: small;
		color: red;
		}

	table.sample {
		border-width: 0px;
		border-spacing: 0px;
		border-style: dashed;
		border-color: gray;
		border-collapse: collapse;
		background-color: white;
	}
	table.sample th {
		height:10px;
		border-width: 1px;
		padding: 4px;
		border-style: groove;
		border-color: black;
		background-color: skyblue;
		-moz-border-radius: ;
	}
	table.sample td {
		border-width: 1px;
		padding: 8px;
		height:10px;
		border-style: groove;
		border-color: black;
		background-color: $bgc;
		-moz-border-radius: ;
	}
</style>

<style type="text/css">
	.TFtable{
		width:40%; 
		border-collapse:collapse; 
	}
	.TFtable td{ 
		padding:0px; border:#4e95f4 1px solid;
	}
	/* provide some minimal visual accomodation for IE8 and below */
	.TFtable tr{
		background: #b8d1f3;
	}
	/*  Define the background color for all the ODD background rows  */
	.TFtable tr:nth-child(odd){ 
		background: #b8d1f3;
	}
	/*  Define the background color for all the EVEN background rows  */
	.TFtable tr:nth-child(even){
		background: #dae5f4;
	}
</style>

</head>

<body>
<br>
<?php 	
	error_reporting (E_ALL ^ E_NOTICE);
	require_once("includes/function.php") ; 
	$cir1="[";
	$cir2="]";
	$tbname1='tag';
	$tbname2='equipment';
	$id=$_GET["id"];
	$strSQL="SELECT n.*, m.aks, m.name, m.breaker, m.location FROM $tbname1 n INNER JOIN $tbname2 m ON (n.aks=m.aks) WHERE n.id = $id";

	$dblink = connect_db() ;
	$resultSql = mysqli_query($dblink, $strSQL ) ;
	$row = mysqli_fetch_assoc($resultSql);
	$affected = mysqli_affected_rows( $dblink ) ;

	if ( $affected != 1 ) {
		$msg = "No complete select data from main SQL" ;
		echo "<br>No data...<br>";
	}else{
		$hangm1=$row["tagm_h1"];
		$hangm2=$row["tagm_h2"];
		$hango1=$row["tago_h1"];
		$hango2=$row["tago_h2"];
		$tagm_outby=$row["tagm_outby"];
		$tago_outby=$row["tago_outby"];
		$userm_addby=$row["tagm_addby"];
		$usero_addby=$row["tago_addby"];
		if(empty($usero_addby)){
			$usero_addby=$row["tailby"];
		}
		$user_hangtag=$row["h_addby"];
		$usero_outby=$row["tagout_addby"];
    
	 	//ผู้จะมาขอแขวน tag ของบำรุงรักษา
		if(!empty($hangm1)){
			$sqlHanger = "SELECT name FROM hanger WHERE egat_id='$hangm1'";
			$resultSqlh = mysqli_query($dblink, $sqlHanger ) ;
			$rowh = mysqli_fetch_assoc($resultSqlh);
			$affectedh = mysqli_affected_rows( $dblink );
			if ( $affectedh != 1 ) {
				$hangm11=$hangm1;
			}else{
				$hangm11=$rowh["name"];
			}
		}else{
			$hangm11="";
		}

	 	//ผู้แขวน tag ของบำรุงรักษา
		if(!empty($hangm2)){
			$sqlHanger = "SELECT name FROM hanger WHERE egat_id='$hangm2'";
			$resultSqlh = mysqli_query($dblink, $sqlHanger ) ;
			$rowh = mysqli_fetch_assoc($resultSqlh);
			$affectedh = mysqli_affected_rows( $dblink ) ;
			if ( $affectedh != 1 ) {
				$hangm22=$hangm2;
			}else{
				$hangm22=$rowh["name"];
			}
		}else{
			$hangm22="";
		}
	
		//ผู้จะมาขอแขวน tag ของเดินเครื่อง
		if(!empty($hango1)){
			$sqlHanger = "SELECT name FROM hanger WHERE egat_id='$hango1'";
			$resultSqlh = mysqli_query($dblink, $sqlHanger ) ;
			$rowh = mysqli_fetch_assoc($resultSqlh);
			$affectedh = mysqli_affected_rows( $dblink ) ;
			if ( $affectedh != 1 ) {
				$hango11=$hango1;
			}else{
				$hango11=$rowh["name"];
			}
		}else{
			$hango11="";
		}

		//ผู้แขวน tag ของเดินเครื่อง
		if(!empty($hango2)){
			$sqlHanger = "SELECT name FROM hanger WHERE egat_id='$hango2'";
			$resultSqlh = mysqli_query($dblink, $sqlHanger ) ;
			$rowh = mysqli_fetch_assoc($resultSqlh);
			$affectedh = mysqli_affected_rows( $dblink ) ;
			if ( $affectedh != 1 ) {
				$hango22=$hangm2;
			}else{
				$hango22=$rowh["name"];
			}
		}else{
			$hango22="";
		}
	
		//ผู้ปลด tag ของบำรุงรักษา
		if(!empty($tagm_outby)){
			$sqlHanger = "SELECT name FROM hanger WHERE egat_id='$tagm_outby'";
			$resultSqlo = mysqli_query($dblink, $sqlHanger ) ;
			$rowo = mysqli_fetch_assoc($resultSqlo);
			$affectedh = mysqli_affected_rows( $dblink ) ;
			if ( $affectedh != 1 ) {
				$tagoutm=$tagm_outby;
			}else{
				$tagoutm=$rowo["name"];
			}
		}else{
			$tagoutm="";
		}

		//ผู้ปลด tag ของเดินเครื่อง
		if(!empty($tago_outby)){
			$sqlHanger = "SELECT name FROM hanger WHERE egat_id='$tago_outby'";
			$resultSqlo = mysqli_query($dblink, $sqlHanger ) ;
			$rowo = mysqli_fetch_assoc($resultSqlo);
			$affectedh = mysqli_affected_rows( $dblink ) ;
			if ( $affectedh != 1 ) {
				$msg = "No complete select data from tagout maintainance";
				$tagouto=$tago_outby;
			}else{
				$tagouto=$rowo["name"];
			}
		}else{
			$tagouto="";
		}

		//ผู้ใข้งานตอนขอแขวน tag บำรุงรักษา
		if(!empty($userm_addby)){
			$sqlHanger = "SELECT name, phone FROM user WHERE egat_id='$userm_addby'";
			$resultSqlo = mysqli_query($dblink, $sqlHanger ) ;
			$rowo = mysqli_fetch_assoc($resultSqlo);
			$affectedh = mysqli_affected_rows( $dblink ) ;
			if ( $affectedh != 1 ) {
				$useraddtagm=$userm_addby;
			}else{
				$useraddtagm=$rowo["name"];
				$useraddphonem=$rowo["phone"];
			}
		}else{
			$useraddtagm="";
		}
	
		//ผู้ใข้งานตอนขอแขวน tag เดินเครื่อง
		if(!empty($usero_addby)){
			$sqlHanger = "SELECT name, phone FROM user WHERE egat_id='$usero_addby'";
			$resultSqlo = mysqli_query($dblink, $sqlHanger ) ;
			$rowo = mysqli_fetch_assoc($resultSqlo);
			$affectedh = mysqli_affected_rows( $dblink ) ;
			if ( $affectedh != 1 ) {
				$useraddtago=$usero_addby;
			}else{
				$useraddtago=$rowo["name"];
				$useraddphoneo=$rowo["phone"];
			}
		}else{
			$useraddtago="";
		}
	
		//ผู้ใข้งานตอนยืนยันแขวน tag เดินเครื่อง
		if(!empty($user_hangtag)){
			$sqlHanger = "SELECT name, phone FROM user WHERE egat_id='$user_hangtag'";
			$resultSqlo = mysqli_query($dblink, $sqlHanger ) ;
			$rowo = mysqli_fetch_assoc($resultSqlo);
			$affectedh = mysqli_affected_rows( $dblink ) ;
			if ( $affectedh != 1 ) {
				$userhangtag=$usero_addby;
			}else{
				$userhangtag=$rowo["name"];
				$userhangphone=$rowo["phone"];
			}
		}else{
			$userhangtag="";
		}

		//ผู้ใข้งานตอนปลด tag เดินเครื่อง
		if(!empty($usero_outby)){
			$sqlHanger = "SELECT name, phone FROM user WHERE egat_id='$usero_outby'";
			$resultSqlo = mysqli_query($dblink, $sqlHanger ) ;
			$rowo = mysqli_fetch_assoc($resultSqlo);
			$affectedh = mysqli_affected_rows( $dblink ) ;
			if ( $affectedh != 1 ) {
				$userouttago=$usero_addby;
			}else{
				$userouttago=$rowo["name"];
				$userouttagphone=$rowo["phone"];
			}
		}else{
			$userouttago="";
		}
?>
<div align="center">
<table class="TFtable">
	<tr align="right">
	<th bgcolor="#CCCC99" style="color:#000000" colspan="2" height="5">Tag Information ID=<?php echo $row["id"]; ?></th></tr>
	<tr>
		<td width="35%" align="right"><small>M-Tag No.&nbsp;:&nbsp;<br />O-Tag No.&nbsp;:&nbsp;</small></td>
		<td align="left"><small>&nbsp;<?php echo $row["tagm_no"]; ?><br />&nbsp;<?php echo $row["tago_no"]; ?></td></tr>
	<tr>
		<td align="right"><small>AKS&nbsp;:&nbsp;<br>Equipment&nbsp;:&nbsp;</td>
		<td align="left"><small>&nbsp;<?php echo $row["aks"]; ?><br />&nbsp;<?php echo $row["name"]; ?></td></tr>
	<tr>
		<td align="right"><small>Breaker&nbsp;:&nbsp;<br>Location&nbsp;:&nbsp;</td>
		<td align="left"><small>&nbsp;<?php echo $row["breaker"]; ?><br />&nbsp;<?php echo $row["location"]; ?></td></tr>
	<tr>
		<td align="right"><small>งานที่ขอทำ&nbsp;:&nbsp;<br>Task&nbsp;:&nbsp;</td>
		<td align="left"><small>&nbsp;<?php echo $row["task"]; ?></td></tr>
	<tr>
		<td align="right"><small>อ้างถึงใบงานเลขที่&nbsp;:&nbsp;<br>Work Order&nbsp;:&nbsp;</td>
		<td align="left"><small>&nbsp;<?php echo $row["workorder"]; ?></td></tr>
	<tr>
		 <td align="right"><small><mm>วันที่ขอแขวน: M&nbsp;:&nbsp;<br><op>วันที่ขอแขวน: O&nbsp;:&nbsp;</td>
		<td align="left"><small>&nbsp;<mm><?php echo $row["tagm_date1"]; ?><br />&nbsp;<op><?php echo $row["tago_date1"]; ?></td></tr>
	<tr>
		<td align="right"><small><mm>ผู้จะแขวน-M&nbsp;:&nbsp;<br><op>ผู้จะแขวน-O&nbsp;:&nbsp;</td>
		<td align="left"><small>&nbsp;<mm><?php echo $row["tagm_h1"]; ?>&nbsp;:&nbsp;<?php echo $hangm11; ?><br />&nbsp;<op><?php echo $row["tago_h1"]; ?>&nbsp;:&nbsp;<?php echo $hango11; ?></td></tr>
	<tr>
		<td align="right"><small><mm>วันที่แขวนจริง:M&nbsp;:&nbsp;<br><op>วันที่แขวนจริง:O&nbsp;:&nbsp;</td>
		<?php if($row['tagm_no']!=""){?>
				<td align="left"><small>&nbsp;<mm><?php echo $row["tagmo_date2"]; ?><br />&nbsp;<op><?php echo $row["tagmo_date2"]; ?></td>
		<?php }else{?>
				<td align="left"><small>&nbsp;<br />&nbsp;<op><?php echo $row["tagmo_date2"]; ?></td>
		<?php }?></tr>
	<tr>
	  <td align="right"><small><mm>ผู้แขวน-M&nbsp;:&nbsp;<br><op>ผู้แขวน-O&nbsp;:&nbsp;</td>
		<td align="left"><small>&nbsp;<mm><?php echo $row["tagm_h2"]; ?>&nbsp;:&nbsp;<?php echo $hangm22; ?><br />&nbsp;<op><?php echo $row["tago_h2"]; ?>&nbsp;:&nbsp;<?php echo $hango22; ?></td>	</tr>
	<tr>
		<td align="right"><small><mm>Key-M&nbsp;:&nbsp;<br><op>Key-O&nbsp;:&nbsp;</td>
		<?php if($row['tagmo_date2']!=""){?>
				<?php if($row['tagm_no']!=""){?>
						<td align="left"><small>&nbsp;<mm><?php echo $row["keym_id"]; ?>&nbsp;<?=($row["keym_status"]!=''?$cir1.$row["keym_status"].$cir2:''); ?><br />&nbsp;<op><?php echo $row["keyo_id"]; ?>&nbsp;<?=($row["keyo_status"]!=''?$cir1.$row["keyo_status"].$cir2:''); ?></td>	
				<?php }else{?>
						<td align="left"><small>&nbsp;<br />&nbsp;<op><?php echo $row["keyo_id"]; ?>&nbsp;<?=($row["keyo_status"]!=''?$cir1.$row["keyo_status"].$cir2:''); ?></td>	
				<?php }?>
		<?php }else{?>
					<td align="left"><small>&nbsp;<mm><?php echo $row["keym_id"]; ?><br />&nbsp;<op><?php echo $row["keyo_id"]; ?></td>	
		<?php }?></tr>			
	<tr>
		<td align="right"><small><mm>วันที่ปลด-M&nbsp;:&nbsp;<br><op>วันที่ปลด-O&nbsp;:&nbsp;</td>
		<td align="left"><small>&nbsp;<mm><?php echo $row["tagm_outdate"]; ?><br />&nbsp;<op><?php echo $row["tago_outdate"]; ?></td></tr>	
	<tr>
		<td align="right"><small><mm>ผู้ปลด-M&nbsp;:&nbsp;<br><op>ผู้ปลด-O&nbsp;:&nbsp;</td>
		<td align="left"><small>&nbsp;<mm><?php echo $row["tagm_outby"]; ?>&nbsp;:&nbsp;<?php echo $tagoutm; ?><br />&nbsp;<op><?php echo $row["tago_outby"]; ?>&nbsp;:&nbsp;<?php echo $tagouto; ?></td></tr>	
	<tr>
		<td align="right"><small><mm>ผู้ใช้งานขอแขวน-M&nbsp;:&nbsp;<br><op>ผู้ใช้งานขอแขวน-O&nbsp;:&nbsp;</td>
		<td align="left"><small>&nbsp;<mm><?php echo $row["tagm_addby"]; ?>&nbsp;:&nbsp;<?php echo $useraddtagm; ?>&nbsp;<?php echo(($useraddtagm!="")?"Tel.":"");?>&nbsp;<?php echo $useraddphonem;?><br />&nbsp;<op><?php echo $usero_addby; ?>&nbsp;:&nbsp;<?php echo $useraddtago; ?>&nbsp;<?php echo(($usero_addby!="")?"Tel.":"");?>&nbsp;<?php echo $useraddphoneo;?></td></tr>
	<tr>
		<td align="right"><small><op>ผู้ใช้งานตอนแขวน-O&nbsp;:&nbsp;</td>
		<td align="left"><small>&nbsp;<op><?php echo $row["h_addby"]; ?>&nbsp;:&nbsp;<?php echo $userhangtag; ?>&nbsp;<?php echo(($userhangtag!="")?"Tel.":"");?>&nbsp;<?php echo $userhangphone;?></td></tr>
	<tr>
		<td align="right"><small><op>ผู้ใช้งานตอนปลด&nbsp;:&nbsp;</td>
		<td align="left"><small>&nbsp;<op><?php echo $row["tagout_addby"]; ?>&nbsp;:&nbsp;<?php echo $userouttago; ?>&nbsp;<?php echo(($userouttago!="")?"Tel.":"");?>&nbsp;<?php echo $userouttagphone;?> </td></tr>
<?php
	}
	mysqli_close($dblink);
	echo '</table>';

?>
</div>
</body>
</html>