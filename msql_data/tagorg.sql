-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Feb 21, 2016 at 02:35 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `tagorg`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `admin`
-- 

CREATE TABLE `admin` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(15) NOT NULL,
  `detail` varchar(50) default NULL,
  `password` varchar(10) NOT NULL,
  `level` varchar(2) NOT NULL,
  `note` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `admin`
-- 

INSERT INTO `admin` VALUES (1, 'admin', 'administrator', 'admin', '99', NULL);

-- --------------------------------------------------------

-- 
-- Table structure for table `ask`
-- 

CREATE TABLE `ask` (
  `id` int(11) NOT NULL auto_increment,
  `ask` longtext NOT NULL,
  `askfile` varchar(30) default NULL,
  `askby` varchar(70) default NULL,
  `asktime` datetime default NULL,
  `askip` varchar(15) default NULL,
  `anwser` longtext,
  `anwserby` varchar(70) default NULL,
  `anwsertime` datetime default NULL,
  `anwserip` varchar(15) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `ask`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `delreport`
-- 

CREATE TABLE `delreport` (
  `id` int(11) NOT NULL auto_increment,
  `tbname` varchar(20) default NULL,
  `idnumber` varchar(10) default NULL,
  `rec1` varchar(70) default NULL,
  `rec2` varchar(70) default NULL,
  `rec3` varchar(70) default NULL,
  `rec4` varchar(70) default NULL,
  `rec5` varchar(70) default NULL,
  `rec6` varchar(70) default NULL,
  `delby` varchar(6) default NULL,
  `deltime` datetime default NULL,
  `delip` varchar(15) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `delreport`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `equipment`
-- 

CREATE TABLE `equipment` (
  `id` int(11) NOT NULL auto_increment,
  `aks` varchar(25) default NULL,
  `name` varchar(200) NOT NULL,
  `breaker` varchar(30) default NULL,
  `location` varchar(200) default NULL,
  `rated` varchar(20) default NULL,
  `note` varchar(200) default NULL,
  `addby` varchar(20) default NULL,
  `addip` varchar(15) default NULL,
  `addtime` datetime default NULL,
  `editby` varchar(20) default NULL,
  `editip` varchar(15) default NULL,
  `edittime` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `equipment`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `hanger`
-- 

CREATE TABLE `hanger` (
  `id` int(11) NOT NULL auto_increment,
  `egat_id` varchar(6) NOT NULL,
  `name` varchar(80) NOT NULL,
  `section_id` varchar(15) NOT NULL,
  `section_thai` varchar(15) default NULL,
  `status` varchar(3) default NULL,
  `phone` varchar(20) default NULL,
  `note` varchar(200) default NULL,
  `addby` varchar(20) default NULL,
  `addtime` datetime default NULL,
  `addip` varchar(15) default NULL,
  `editby` varchar(20) default NULL,
  `edittime` datetime default NULL,
  `editip` varchar(15) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `hanger`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `org`
-- 

CREATE TABLE `org` (
  `id` int(11) NOT NULL auto_increment,
  `sec_id` varchar(15) NOT NULL,
  `name` varchar(20) NOT NULL,
  `sec_detail` varchar(100) default NULL,
  `dep_name` varchar(20) NOT NULL,
  `dep_detail` varchar(100) default NULL,
  `div_name` varchar(20) NOT NULL,
  `div_detail` varchar(100) default NULL,
  `institute_name` varchar(20) default NULL,
  `institute_detail` varchar(100) default NULL,
  `addby` varchar(20) default NULL,
  `addtime` datetime default NULL,
  `addip` varchar(15) default NULL,
  `editby` varchar(20) default NULL,
  `edittime` datetime default NULL,
  `editip` varchar(15) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `org`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `owner`
-- 

CREATE TABLE `owner` (
  `id` int(11) NOT NULL auto_increment,
  `location` varchar(150) NOT NULL,
  `unit_id` varchar(10) default NULL,
  `owner` varchar(150) NOT NULL,
  `phone` varchar(50) default NULL,
  `note` varchar(150) default NULL,
  `addby` varchar(20) default NULL,
  `addip` varchar(15) default NULL,
  `addtime` datetime default NULL,
  `editby` varchar(20) default NULL,
  `editip` varchar(15) default NULL,
  `edittime` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `owner`
-- 

INSERT INTO `owner` VALUES (2, 'Mae Moh Power Plant Unit11', 'MM-T11', 'กองการผลิต2 ฝ่ายการผลิต โรงไฟฟ้าแม่เมาะ ลำปาง', 'โทรฯ 054-252-321 แฟกซ์ 054-252-317', 'กผม2-ฟ. อฟม.', 'admin', '10.249.91.168', '2015-01-09 06:41:02', 'admin', '10.249.91.168', '2015-01-09 06:41:40');

-- --------------------------------------------------------

-- 
-- Table structure for table `suggestion`
-- 

CREATE TABLE `suggestion` (
  `id` int(11) NOT NULL auto_increment,
  `quiz` varchar(200) default NULL,
  `anwser` varchar(200) default NULL,
  `quizby` varchar(70) default NULL,
  `quiztime` datetime default NULL,
  `quizip` varchar(15) default NULL,
  `anwserby` varchar(70) default NULL,
  `anwsertime` datetime default NULL,
  `anwserip` varchar(15) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `suggestion`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tag`
-- 

CREATE TABLE `tag` (
  `id` int(11) NOT NULL auto_increment,
  `aks` varchar(25) NOT NULL,
  `task` varchar(200) NOT NULL,
  `tagm_no` varchar(25) default NULL,
  `tagm_org` varchar(6) default NULL,
  `tagm_date1` date default NULL,
  `tagm_h1` varchar(6) default NULL,
  `tagm_addby` varchar(6) default NULL,
  `tagm_addtime` datetime default NULL,
  `tagm_addip` varchar(15) default NULL,
  `tagm_editby` varchar(6) default NULL,
  `tagm_edittime` datetime default NULL,
  `tagm_editip` varchar(15) default NULL,
  `tago_no` varchar(30) default NULL,
  `tago_org` varchar(6) default NULL,
  `tago_date1` date default NULL,
  `tago_h1` varchar(6) default NULL,
  `tago_addby` varchar(6) default NULL,
  `tago_addtime` datetime default NULL,
  `tago_addip` varchar(15) default NULL,
  `tago_editby` varchar(6) default NULL,
  `tago_edittime` datetime default NULL,
  `tago_editip` varchar(15) default NULL,
  `tagmo_date2` date default NULL,
  `tagm_h2` varchar(6) default NULL,
  `tago_h2` varchar(6) default NULL,
  `keym_id` varchar(15) default NULL,
  `keym_status` varchar(3) default NULL,
  `keyo_id` varchar(15) default NULL,
  `keyo_status` varchar(3) default NULL,
  `workorder` varchar(15) default NULL,
  `h_addby` varchar(6) default NULL,
  `h_addtime` datetime default NULL,
  `h_addip` varchar(15) default NULL,
  `h_editby` varchar(6) default NULL,
  `h_edittime` datetime default NULL,
  `h_editip` varchar(15) default NULL,
  `tagm_outby` varchar(6) default NULL,
  `tagm_outdate` date default NULL,
  `tago_outby` varchar(6) default NULL,
  `tago_outdate` date default NULL,
  `tagout_addby` varchar(6) default NULL,
  `tagout_addtime` datetime default NULL,
  `tagout_addip` varchar(15) default NULL,
  `tagout_editby` varchar(6) default NULL,
  `tagout_edittime` datetime default NULL,
  `tagout_editip` varchar(15) default NULL,
  `cancel_status` varchar(3) default NULL,
  `cancel_note` varchar(50) default NULL,
  `cancel_by` varchar(6) default NULL,
  `cancel_time` datetime default NULL,
  `cancel_ip` varchar(15) default NULL,
  `del_status` varchar(3) default NULL,
  `del_note` varchar(50) default NULL,
  `del_by` varchar(6) default NULL,
  `del_time` datetime default NULL,
  `del_ip` varchar(6) default NULL,
  `tagtest_outdate` date default NULL,
  `tagtestm_outby` varchar(6) default NULL,
  `tagtesto_outby` varchar(6) default NULL,
  `tagtestout_addby` varchar(6) default NULL,
  `tagtestout_addtime` datetime default NULL,
  `tagtestout_addip` varchar(15) default NULL,
  `tagtestout_editby` varchar(6) default NULL,
  `tagtestout_edittime` datetime default NULL,
  `tagtestout_editip` varchar(15) default NULL,
  `tagtestm_hby` varchar(6) default NULL,
  `tagtesto_hby` varchar(6) default NULL,
  `tagtestmo_hdate` date default NULL,
  `tagtesth_addby` varchar(6) default NULL,
  `tagtesth_addtime` datetime default NULL,
  `tagtesth_addip` varchar(15) default NULL,
  `tagtesth_editby` varchar(6) default NULL,
  `tagtesth_edittime` datetime default NULL,
  `tagtesth_editip` varchar(15) default NULL,
  `note` varchar(100) default NULL,
  `tailby` varchar(6) default NULL,
  `tailtime` datetime default NULL,
  `tailip` varchar(15) default NULL,
  `tagm_status` varchar(5) default NULL,
  `tago_status` varchar(5) default NULL,
  `printtago` varchar(1) default NULL,
  `printtimeo` datetime default NULL,
  `printipo` varchar(15) default NULL,
  `printtagm` varchar(1) default NULL,
  `printtimem` datetime default NULL,
  `printipm` varchar(15) default NULL,
  `printsign` varchar(1) default NULL,
  `printsigntime` datetime default NULL,
  `printsignip` varchar(15) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='tag' AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `tag`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `user`
-- 

CREATE TABLE `user` (
  `id` int(11) NOT NULL auto_increment,
  `egat_id` varchar(6) default NULL,
  `name` varchar(150) character set tis620 default NULL,
  `password` varchar(10) default NULL,
  `status` varchar(2) default NULL,
  `section_id` varchar(15) default NULL,
  `section_thai` varchar(15) default NULL,
  `phone` varchar(20) default NULL,
  `note` varchar(200) default NULL,
  `addby` varchar(25) default NULL,
  `addtime` datetime default NULL,
  `addip` varchar(15) default NULL,
  `editby` varchar(25) default NULL,
  `edittime` datetime default NULL,
  `editip` varchar(15) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `user`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `userlog`
-- 

CREATE TABLE `userlog` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(80) NOT NULL,
  `logintime` datetime default NULL,
  `loginip` varchar(15) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `userlog`
-- 

