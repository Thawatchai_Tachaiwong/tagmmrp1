<?php  //20-12-2019 by anek suriwongyai
    session_start(); //session_destroy();
    require_once("includes/function.php");
    include('connexion.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Report Form MF-00-ASC-37-01-02</title>

    <link rel="stylesheet" href="dist/css/bootstrap.min.css">

    <!--*********Start calendar************-->
    <link type="text/css" href="jquery/flora.calendars.picker.css" rel="stylesheet"/> 
    <script type="text/javascript" src="jquery/jquery.min.js"></script> 
    <script type="text/javascript" src="jquery/jquery.calendars.min.js"></script> 
    <script type="text/javascript" src="jquery/jquery.calendars.plus.min.js"></script> 
    <script type="text/javascript" src="jquery/jquery.calendars.picker.min.js"></script> 
    <script type="text/javascript" src="jquery/jquery.calendars.thai.min.js"></script> 
    <script type="text/javascript" src="jquery/jquery.calendars.thai-th.js"></script> 
    <script type="text/javascript" src="jquery/jquery.calendars.picker-th.js"></script> 



    <style media="all" type="text/css">
        .round_image{
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
        border-radius: 10px;
        -webkit-box-shadow: #000 0 5px 10px;
        -moz-box-shadow: #000 0 5px 10px;
        box-shadow: #000 0 5px 10px;
        }
    </style>
	<style>
		#pagination div { display: inline-block; margin-right: 5px; margin-top: 5px }
		#pagination .cell a { border-radius: 3px; font-size: 11px; color: #333; padding: 8px; text-decoration:none; border: 1px solid #d3d3d3; background-color: #f8f8f8; }
		#pagination .cell a:hover { border: 1px solid #c6c6c6; background-color: #f0f0f0;  }
		#pagination .cell_active span { border-radius: 3px; font-size: 11px; color: #ccc; padding: 8px; border: 1px solid #c6c6c6; background-color: #e9e9e9; }
		#pagination .cell_disabled span { border-radius: 3px; font-size: 11px; color: #777777; padding: 8px; border: 1px solid #dddddd; background-color: #ffffff; }
    </style>
    <style>
        body {
            background-color: #eee;
        }
    </style>
</head>

<body>
    <script type="text/javascript"> 
        $(function() {     
            $('#mydate1').calendarsPicker({calendar: $.calendars.instance('thai','th')}); 
            $('#mydate2').calendarsPicker({calendar: $.calendars.instance('thai','th')}); 
        });
    </script>

    <?php
        $strSearch=""; 
        if(isset($_POST['submit'])){
            if(!empty($_POST['txtSearch']) && !empty($_POST['mydate1']) && !empty($_POST['mydate2'])){
                $txtSearch = $_POST['txtSearch'];
                $txtSearch1 = thaiDate($_POST['mydate1']);
                $txtSearch2 = thaiDate($_POST['mydate2']);

                $strSearch = '((n.tagm_no LIKE "%'.$txtSearch.'%" ';
                $strSearch .= 'OR n.tago_no LIKE "%'.$txtSearch.'%") ';
                $strSearch .= 'AND (n.tago_date1 >= "'.$txtSearch1.'" ';
                $strSearch .= 'AND n.tago_date1 <= "'.$txtSearch2.'")) ';
            }elseif(!empty($_POST['txtSearch']) && !empty($_POST['mydate1']) && empty($_POST['mydate2'])){
                $txtSearch = $_POST['txtSearch'];
                $txtSearch1 = thaiDate($_POST['mydate1']);

                $strSearch = '(n.tagm_no LIKE "%'.$txtSearch.'%" ';
                $strSearch .= 'OR n.tago_no LIKE "%'.$txtSearch.'%") ';
                $strSearch .= 'AND (n.tago_date1 >= "'.$txtSearch1.'")) ';
            }elseif(!empty($_POST['txtSearch']) && empty($_POST['mydate1']) && !empty($_POST['mydate2'])){
                $txtSearch = $_POST['txtSearch'];
                $txtSearch1 = thaiDate($_POST['mydate2']);

                $strSearch = '(n.tagm_no LIKE "%'.$txtSearch.'%" ';
                $strSearch .= 'OR n.tago_no LIKE "%'.$txtSearch.'%") ';
                $strSearch .= 'AND (n.tago_date1 <= "'.$txtSearch1.'")) ';
            }elseif(empty($_POST['txtSearch']) && !empty($_POST['mydate1']) && !empty($_POST['mydate2'])){
                $txtSearch1 = thaiDate($_POST['mydate1']);
                $txtSearch2 = thaiDate($_POST['mydate2']);

                $strSearch = '(n.tago_date1 >= "'.$txtSearch1.'" ';
                $strSearch .= 'AND n.tago_date1 <= "'.$txtSearch2.'") ';
            }elseif(!empty($_POST['txtSearch']) && empty($_POST['mydate1']) && empty($_POST['mydate2'])){
                $txtSearch = $_POST['txtSearch'];

                $strSearch = '(n.tagm_no LIKE "%'.$txtSearch.'%" ';
                $strSearch .= 'OR n.tago_no LIKE "%'.$txtSearch.'%") ';
            }elseif(empty($_POST['txtSearch']) && !empty($_POST['mydate1']) && empty($_POST['mydate2'])){
                $txtSearch = thaiDate($_POST['mydate1']);

                $strSearch = '(n.tago_date1 >= "'.$txtSearch.'") ';
            }elseif(empty($_POST['txtSearch']) && empty($_POST['mydate1']) && !empty($_POST['mydate2'])){
                $txtSearch = thaiDate($_POST['mydate2']);

                $strSearch = '(n.tago_date1 <= "'.$txtSearch.'") ';
            }
            if($strSearch != ""){
                $_SESSION['strSearch']=$strSearch;
                // echo '<br>Text search = '.$strSearch;
            }else{
                $_SESSION['strSearch'] = "";
            }
        }else{
            $_SESSION['strSearch'] = "";
        }
    ?>

    <div class="container-fluid px-0 mt-0 pt-0">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 mx-auto ">
                <table class="table" border=1>
                    <tr class="tr text-right bg-success"><td colspan=11>
                    <form action="" method="post">
                        Search : <input type="text" name="txtSearch" value="" id="txtSearch" >
                        &nbsp;Start time : <input type="text" name="mydate1" value="" id="mydate1" >&nbsp;End time : <input type="text" name="mydate2" value="" id="mydate2" >&nbsp;	
                        <input type="submit" name="submit" value="Submit">
                    </form>
                    </td></tr>
                    
                    <?php include('table_head.php');?> 
                </table>
                <div id="articleArea"></div>
            </div>
        </div>
    </div>

    <div class="container-fluid px-0 p-0">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 mx-auto">
                <!-- <div id="articleArea"></div>  -->
                <!-- Where articles appear -->
               
            <!-- Where pagination appears -->
                <div id="pagination">
            <!-- Just tell the system we start with page 1 (id=1) -->
            <!-- See the .js file, we trigger a click when page is loaded -->
                    <div><a href="#" id="1"></a></div>
                </div>
            </div>
        </div>
    </div>


    <!-- <script src="js/jquery-1.11.2.min.js"></script> -->
    <script src="js/reportmf-pagination.js"></script>
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- <script src="popper.js/dist/umd/popper.min.js"></script> -->

    
</body>
</html>