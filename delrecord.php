<?php    /**By Anek suriwongyai 11-05-2562 */
    session_start();
    //require_once("includes/function.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<script src="./js/jquery.min.js"></script>

<link rel="stylesheet" href="./css/bootstrap.min.css" media="all">
<link rel='stylesheet' type='text/css' href='./css/style.css'>
<title>Delete record progress</title>
<link rel="stylesheet" type="text/css" href="dist/jquery.dataTables.min.css"/>	 
<script type="text/javascript" src="dist/jquery.dataTables.min.js"></script>

<style media="all" type="text/css">
	.round_image{
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		-webkit-box-shadow: #000 0 2px 10px;
		-moz-box-shadow: #000 0 2px 10px;
		box-shadow: #000 0 2px 10px;
	}
</style>

<style>
	@media (min-width: 500px) {
			.container{
				width: 800px;
                align-content: center;
			}
	}
</style>
 
</head>

<body>
<?php require_once("navbar_index.php");?>
<?php
	if(ISSET($_GET['id'])){
		$tblname=$_GET['tb'];
		$id=$_GET['id'];
		$url=$_GET['url'];
	}else{
		echo '<br><br><center><span style="background-color:yellow">&nbsp;No data to delete...!</span></center>';
		exit();
	}

	$strSql="SELECT * FROM $tblname WHERE id =".$id;
	include_once("connect_db.php");
	$result=$mysqli->query($strSql);
	$total_rows = $result->num_rows;
	// if($result->num_rows>0){			

	if($total_rows<1){			
		echo '<br>No data...<br>';
		exit();
	}else{
		$row = $result->fetch_array(MYSQLI_NUM);
		//$row = $result->fetch_array(MYSQLI_BOTH);
		$rec1=$row[1];
		$rec2=$row[2];
		$rec3=$row[3];
		$rec4=$row[13];
		$rec5=$row[23];
		$rec6=$row[40];
		$tbname="delreport";
		$deluser=$_SESSION["EGATID"];
		$gdate=date("Y-m-d H:i:s");
		$clientip=$_SERVER["REMOTE_ADDR"];
		if($clientip=="::1"){
				$clientip="127.0.0.1";
			}
		$insert = "INSERT INTO $tbname (tbname, idnumber, rec1,rec2, rec3, rec4, rec5, rec6, delby, deltime, delip)";
		$insert .=" VALUES ('".$tblname."', '".$id."', '".$rec1."', '".$rec2."', '".$rec3."', '".$rec4."', '".$rec5."', '".$rec6."', '".$deluser."', '".$gdate."', '".$clientip."')" ;

		$mysqli->query("SET NAMES 'utf8'");
		$result = $mysqli->query($insert);
		$affected_rows = $mysqli->affected_rows;

		$strSql="DELETE FROM $tblname WHERE id =".$id;
		$result = $mysqli->query($strSql);
		$mysqli->close();

		if ( $result) {
			if(@$affected_rows==1){
				$msg = "Deleted record and update data successful...";
			}else{
				$msg = "Deleted record successful but cannot update data..!";
			}
            echo '<center><br /><br /><table width="60%" border="0" cellspacing="0"><tr bgcolor="#eee" height="110"><td align="center">';
            echo '<br/><font face="MS Sans Serif"><font color="red" size="+1">'.$msg.'</font></font><br/>' ;
            echo '<img src="images/process.gif"/><br/>' ;	
            echo '<meta http-equiv="refresh" content="2; URL='.$url.'">' ;
            echo '</tr></td></table></center>';
		} else {
			if(@$affected_rows==1){
				$msg = "Can not Deleted data...! but update data successful.";
			}else{
				$msg = "Can not Deleted and Update data..!";
			}
            echo '<center><br /><br /><table width="40%" border="0" cellspacing="0"><tr bgcolor="#eee" height="110"><td align="center">';
            echo '<br/><font face="MS Sans Serif"><font color="red" size="+1">'.$msg.'</font></font><br/>' ;
            echo '<img src="images/process.gif"/><br/>' ;	
            echo '<meta http-equiv="refresh" content="2; URL='.$url.'">' ;
            echo '</tr></td></table></center>';
		}
	}        
?>

</body>
</html>