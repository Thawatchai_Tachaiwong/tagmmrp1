<?php  
function connect_db( ) {
	$link = mysqli_connect( "localhost", "root", "451452", "tagmmrp1" ) ; //แก้ไข xxxx ให้ถูกต้อง
	if ( $link === false ) {   
		die( "เกิดข้อผิดพลาดในการเชื่อมต่อฐานข้อมูล" ) ;			
	} else {
		mysqli_query( $link, "SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'" );
		return $link ;
	} 
}  

function process_message( $msg, $url ) { 
	$info = pathinfo( $url );
	echo '<br /><table align="center" bgcolor="#990000" width="50%">' ;
	echo '<tr><td bgcolor="#FF9999" align="center" height="30" style="font-weight:bold; font-size:16px">RESULT</td></tr>' ;
	echo '<tr><td bgcolor="#FCFCFC" align="center">' ;
	echo '<br/><font color="red" size="+1">'.$msg.'</font><br/>' ;
	echo '<img src="'.ROOT_DIR.'images/process.gif"/><br/>' ;
	echo '<font color="blue" size="+1">Please wait..</font>' ;
	echo '<br/><br/></td></tr></table><br/>' ;  
	echo '<meta http-equiv="refresh" content="2; URL='.$url.'">' ;  
	show_footer( ) ;
	exit ; 
} 

function owner() { 
	global $strHeader; 
	$strOwner; $strPhone;
	$strSQL="SELECT * FROM owner order by id DESC LIMIT 0, 1";
	$dblink = connect_db() ;
	$result = mysqli_query( $dblink, $strSQL ) ;
	$Num_Rows = mysqli_num_rows( $result );
	if ( $Num_Rows == 1 ) {  			
		$row = mysqli_fetch_array( $result, MYSQL_ASSOC );
		$strHeader=$row["location"]; 
		$strOwner=$row["owner"]; 
		$strPhone=$row["phone"]; 
		mysqli_close($dblink); 
		return;
	} else { 
		mysqli_close($dblink); 
	}
}

function check_authen( $right ) {
	if ( $right == "admin" && !isset($_SESSION["Admin"]) ) {
	 	$msg = "ส่วนนี้ใช้งานได้เฉพาะผู้ดูแลระบบ <br/>หากคุณเป็นผู้ดูแลระบบ กรุณาเข้าระบบก่อน";
		process_message ($msg, ROOT_DIR."index.php") ; 
		show_footer() ; 		exit ;

	} elseif ( $right == "member" && !isset($_SESSION["ssMember"]) ) {
	 	$msg = "ส่วนนี้ใช้งานได้เฉพาะสมาชิกของเว็บไซต์ <br/>" ; 
		$msg .= "<ul style=\"text-align:left; font-size:20px\"><li>หากคุณเป็นสมาชิก กรุณาเข้าระบบก่อน</li>" ; 
		$msg .= "<li>หากคุณไม่ได้เป็นสมาชิก กรุณาสมัครสมาชิกก่อน</li></ul>" ;
		process_message ($msg, ROOT_DIR."index.php") ; 
		show_footer() ; 		exit ;

	// user ในที่นี้หมายถึง admin และ member 	
	} elseif ( $right == "user" && ( !isset($_SESSION["Admin"]) && !isset($_SESSION["ssMember"]) ) ) { 
		$msg = "ส่วนนี้ใช้งานได้เฉพาะผู้ดูแลระบบ และสมาชิกของเว็บไซต์ <br/>หากคุณเป็นผู้ดูแลระบบ หรือสมาชิก กรุณาเข้าระบบก่อน" ;
		process_message( $msg, ROOT_DIR."index.php" ) ; 			
		show_footer() ; 		
		exit ;
	}
}

function ascii_unicode_thai($text_input) {
	$text_output = "";
	for ($i=0;$i<strlen($text_input);$i++) {
	if (ord($text_input[$i])<=126)
	$text_output .= $text_input[$i];
	else
	$text_output .= "&#".(ord($text_input[$i])-161+3585).";";
	}
	return $text_output;
}
/*
function utf8_to_tis620($string) {
	$str = $string;
	$res = "";
	for ($i = 0; $i < strlen($str); $i++) {
		if (ord($str[$i]) == 224) {
			$unicode = ord($str[$i+2]) & 0x3F;
			$unicode |= (ord($str[$i+1]) & 0x3F) << 6;
			$unicode |= (ord($str[$i]) & 0x0F) << 12;
			$res .= chr($unicode-0x0E00+0xA0);
			$i += 2;
		} else {
			$res .= $str[$i];
		}
	}
	return $res;
}

function tis620_to_utf8($tis) {
	for( $i=0 ; $i< strlen($tis) ; $i++ ){
		$s = substr($tis, $i, 1);
		$val = ord($s);
		if( $val < 0x80 ){
			$utf8 .= $s;
		} elseif ((0xA1 <= $val and $val <= 0xDA) || (0xDF <= $val and $val <= 0xFB)) {
			$unicode = 0x0E00 + $val - 0xA0;
			$utf8 .= chr( 0xE0 | ($unicode >> 12) );
			$utf8 .= chr( 0x80 | (($unicode >> 6) & 0x3F) );
			$utf8 .= chr( 0x80 | ($unicode & 0x3F) );
		}
	}
	return $utf8;
}
*/
function thaiDate($datetime) {
	@list($date,$time) =  explode(' ',$datetime);
	@list($H,$i,$s) = explode(':',$time);
	@list($d,$m,$Y) = explode('/',$date);

	if($Y>2500){
		$Y = $Y-543;
	} 
	return $Y."-".$m."-".$d;
}

function thaiDate1($datetime) {
	list($date,$time) = split(' ',$datetime); // แยกวันที่ กับ เวลาออกจากกัน
	list($H,$i,$s) = split(':',$time); // แยกเวลา ออกเป็น ชั่วโมง นาที วินาที
	list($d,$m,$Y) = split('/',$date); // แยกวันเป็น ปี เดือน วัน
	if($Y>2500){
		$Y = $Y-543;
	}
	//$Y = $Y-543; 
	return $Y."-".$m."-".$d;
}

function thaiDate2($datetime) {
	@list($date,$time) = explode(' ',$datetime); // แยกวันที่ กับ เวลาออกจากกัน
	list($H,$i,$s) = explode(':',$time); // แยกเวลา ออกเป็น ชั่วโมง นาที วินาที
	list($d,$m,$Y) = explode('/',$date); // แยกวันเป็น ปี เดือน วัน
	$Y = $Y-543; 
	$dd=15;
	return $Y."-".$m."-".$dd;
}

function thaiDate3($datetime) {
	@list($date,$time) = explode(' ',$datetime); // แยกวันที่ กับ เวลาออกจากกัน
	@list($H,$i,$s) = explode(':',$time); // แยกเวลา ออกเป็น ชั่วโมง นาที วินาที
	@list($Y,$m,$d) = explode('-',$date); // แยกวันเป็น ปี เดือน วัน
	if($Y>2500){
		$Y = $Y-543;
	}
	switch($m) {
		case "01":	$m = "ม.ค."; break;
		case "02":	$m = "ก.พ."; break;
		case "03":	$m = "มี.ค."; break;
		case "04":	$m = "เม.ย."; break;
		case "05":	$m = "พ.ค."; break;
		case "06":	$m = "มิ.ย."; break;
		case "07":	$m = "ก.ค."; break;
		case "08":	$m = "ส.ค."; break;
		case "09":	$m = "ก.ย."; break;
		case "10":	$m = "ต.ค."; break;
		case "11":	$m = "พ.ย."; break;
		case "12":	$m = "ธ.ค."; break;
	}
		return $d."     ".$m."    ".$Y;
}

function date_to_stamp( $date, $slash_time = true, $timezone = 'Europe/London', $expression = "#^\d{2}([^\d]*)\d{2}([^\d]*)\d{4}$#is" ) {
    $return = false;
    $_timezone = date_default_timezone_get();
    date_default_timezone_set( $timezone );
    if( preg_match( $expression, $date, $matches ) )
        $return = date( "Y-m-d " . ( $slash_time ? '00:00:00' : "h:i:s" ), strtotime( str_replace( array($matches[1], $matches[2]), '-', $date ) . ' ' . date("h:i:s") ) );
    date_default_timezone_set( $_timezone );
    return $return;
}

function getTagno($tagno) {
	$tagsplit=explode(":",$tagno);
	$splitint=$tagsplit[1];
	$tagnoint=(int)$splitint+1;
	$tagno=$tagsplit[0].':'.$tagnoint;
	return $tagno;
}
?>