<?php //11-05-2019 by anek suriwongyai
    session_start(); //session_destroy();
    if ( ISSET($_SESSION['level'])){
      if($_SESSION['level']!=="55" && $_SESSION['level']!=="99"){
        echo '<br><br><center><span style="background-color:yellow">&nbsp;Access denied...! Not permissive this feature.</span>';
        echo '<br><br><a href="../../login.php">Continous</a>&nbsp;|&nbsp;<a href="../../equipment.php">Equipment page click here</a></center>';
        exit();
      }
    }else{
        echo '<br><br><center><span style="background-color:yellow">&nbsp;Access denied...! Not permissive this feature.</span>';
        echo '<br><br><a href="../../login.php">Continous</a>&nbsp;|&nbsp;<a href="../../equipment.php">Equipment page click here</a></center>';
        exit();
    }
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Tag unserialize</title>
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen">
		<link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen">
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
		<link rel="stylesheet" href="css/font-awesome-4.7.0/css/font-awesome.min.css" type="text/css" media="screen">
  </head>
	
  <body>
    <div id="message"></div>
		<div id="wrap">
      <h2><a href="../../">HOME</a></h2><h1> Edit Tag User</h1> 
      <!-- Feedback message zone -->
      <div id="toolbar">
        <input type="text" id="filter" name="filter" placeholder="Filter :type any text here"  />
        <a id="showaddformbutton" class="button green"><i class="fa fa-plus"></i> Add new row</a>
      </div>
      
      <!-- Grid contents -->
      <div id="tablecontent"></div>
    
      <!-- Paginator control -->
      <div id="paginator"></div>
		</div>  

		<script src="js/jquery-1.11.1.min.js" ></script>
		<script src="js/editablegrid-2.1.0-49.js"></script>   
    <!-- EditableGrid test if jQuery UI is present. If present, a datepicker is automatically used for date type -->
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<script src="js/tbgrid.js" ></script>

		<script type="text/javascript">
      var datagrid; 
      window.onload = function() { 
        datagrid = new DatabaseGrid();

          // key typed in the filter field
          $("#filter").keyup(function() {
            datagrid.editableGrid.filter( $(this).val());

              // To filter on some columns, you can set an array of column index 
              //datagrid.editableGrid.filter( $(this).val(), [0,3,5]);
            });

          $("#showaddformbutton").click( function()  {
            showAddForm();
          });
          $("#cancelbutton").click( function() {
            showAddForm();
          });

          $("#addbutton").click(function() {
            datagrid.addRow();
          });          
	    } 

      $(function () { 

        });

		</script>
    <!-- simple form, used to add a new row -->
    <div id="addform">
        <div class="row">
          <input type="text" id="egat_id" name="egat_id" placeholder="egat id" />
        </div>
        <div class="row">
          <input type="text" id="name" name="name" placeholder="thai name" />
        </div>
        <!-- <div class="row">
          <input type="text" id="password" name="password" placeholder="password" />
        </div> -->

        <div class="row tright">
          <a id="addbutton" class="button green" ><i class="fa fa-save"></i> Apply</a>
          <a id="cancelbutton" class="button delete">Cancel</a>
        </div>
    </div>
	</body>
</html>