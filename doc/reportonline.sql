SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `reportonline` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `reportonline` ;

-- -----------------------------------------------------
-- Table `reportonline`.`tbcom`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `reportonline`.`tbcom` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `report_title` VARCHAR(200) NULL COMMENT 'หัวข้อเรื่อง' ,
  `report_by` VARCHAR(70) NULL COMMENT 'ผู้ออกบันทึก' ,
  `report_to` VARCHAR(45) NULL COMMENT 'เรียนถึง' ,
  `report_pass` VARCHAR(45) NULL COMMENT 'ผ่าน' ,
  `report_detail` LONGTEXT NULL COMMENT 'รายละเอียด' ,
  `report_regards` VARCHAR(45) NULL ,
  `report_attach01` VARCHAR(70) NULL COMMENT 'สิ่งแนบที่ 1' ,
  `report_attach02` VARCHAR(70) NULL COMMENT 'สิ่งแนบที่ 2' ,
  `report_attach03` VARCHAR(70) NULL COMMENT 'สิ่งแนบที่ 3' ,
  `report_attach04` VARCHAR(70) NULL COMMENT 'สิ่งแนบที่ 4' ,
  `report_attach05` VARCHAR(70) NULL COMMENT 'สิ่งแนบที่ 5' ,
  `report_addtime` DATETIME NULL COMMENT 'เวลาบันทึก' ,
  `report_addby` VARCHAR(45) NULL COMMENT 'ผู้ใช้งาน' ,
  `report_addip` VARCHAR(45) NULL COMMENT 'บันทึกจากเครื่อง ip address' ,
  `report_editby` VARCHAR(45) NULL ,
  `report_edittime` DATETIME NULL ,
  `report_editip` VARCHAR(45) NULL ,
  `comment01_by` VARCHAR(70) NULL COMMENT 'unit comment name' ,
  `comment01_to` VARCHAR(45) NULL COMMENT 'unit เรียน' ,
  `comment01_pass` VARCHAR(70) NULL COMMENT 'unit ผ่าน' ,
  `comment01` LONGTEXT NULL COMMENT 'unit comment' ,
  `comment01_regards` VARCHAR(45) NULL COMMENT 'unit comment regards' ,
  `comment01_addtime` DATETIME NULL ,
  `comment01_addip` VARCHAR(45) NULL ,
  `comment01_editby` VARCHAR(45) NULL ,
  `comment01_edittime` DATETIME NULL ,
  `comment01_editip` VARCHAR(45) NULL ,
  `comment02_by` VARCHAR(70) NULL COMMENT 'shiftcharge comment name' ,
  `comment02_degree` VARCHAR(45) NULL COMMENT 'ตำแหน่งระกับกอง' ,
  `comment02_to` VARCHAR(45) NULL COMMENT 'shiftcharge เรียน' ,
  `comment02_pass` VARCHAR(70) NULL COMMENT 'shiftcharge ผ่าน' ,
  `comment02` LONGTEXT NULL COMMENT 'shiftcharge comment' ,
  `comment02_regards` VARCHAR(45) NULL COMMENT 'shiftchargecomment regards' ,
  `comment02_addtime` DATETIME NULL ,
  `comment02_addip` VARCHAR(45) NULL ,
  `comment02_editby` VARCHAR(45) NULL ,
  `comment02_edittime` DATETIME NULL ,
  `comment02_editip` VARCHAR(45) NULL ,
  `comment03_by` VARCHAR(70) NULL COMMENT 'ชื่อกอง บันทึก' ,
  `comment03_degree` VARCHAR(45) NULL COMMENT 'ตำแหน่งระกับกอง' ,
  `comment03_to` VARCHAR(45) NULL COMMENT 'กอง เรียน' ,
  `comment03` LONGTEXT NULL COMMENT 'กอง comment' ,
  `comment03_regards` VARCHAR(45) NULL COMMENT 'shiftchargecomment regards' ,
  `comment03_mailto` VARCHAR(45) NULL ,
  `comment03_addtime` DATETIME NULL ,
  `comment03_addip` VARCHAR(45) NULL ,
  `comment03_editby` VARCHAR(45) NULL ,
  `comment03_edittime` DATETIME NULL ,
  `comment03_editip` VARCHAR(45) NULL ,
  `response_by` VARCHAR(45) NULL ,
  `response_to` VARCHAR(45) NULL ,
  `response_detail` LONGTEXT NULL ,
  `response_regards` VARCHAR(45) NULL ,
  `response_attach` VARCHAR(100) NULL ,
  `response_mailto` VARCHAR(45) NULL ,
  `response_addtime` DATETIME NULL ,
  `response_addip` VARCHAR(45) NULL ,
  `response_editby` VARCHAR(45) NULL ,
  `response_edittime` DATETIME NULL ,
  `response_editip` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

USE `reportonline` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
