<?php
require_once('config.php');         

// Database connection                                   
$mysqli = mysqli_init();
$mysqli->options(MYSQLI_OPT_CONNECT_TIMEOUT, 5);
$mysqli->real_connect($config['db_host'],$config['db_user'],$config['db_password'],$config['db_name']); 

// Get all parameter provided by the javascript
$aks = $mysqli->real_escape_string(strip_tags($_POST['aks']));
$name = $mysqli->real_escape_string(strip_tags($_POST['name']));
$tablename = $mysqli->real_escape_string(strip_tags($_POST['tablename']));

$return=false;
$mysqli->query("SET NAMES 'utf8'");
if ( $stmt = $mysqli->prepare("INSERT INTO ".$tablename."  (aks, name) VALUES (  ?, ?)")) {

	$stmt->bind_param("ss", $aks, $name);
    $return = $stmt->execute();
	$stmt->close();
}             
$mysqli->close();        

echo $return ? "ok" : "error";