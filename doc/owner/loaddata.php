<?php //11-05-2019 by anek suriwongyai
	require_once('config.php');      
	require_once('EditableGrid.php');            

function fetch_pairs($mysqli,$query){
	if (!($res = $mysqli->query($query)))return FALSE;
	$rows = array();
	while ($row = $res->fetch_assoc()) {
		$first = true;
		$key = $value = null;
		foreach ($row as $val) {
			if ($first) { $key = $val; $first = false; }
			else { $value = $val; break; } 
		}
		$rows[$key] = $value;
	}
	return $rows;
}

// Database connection
$mysqli = mysqli_init();
$mysqli->options(MYSQLI_OPT_CONNECT_TIMEOUT, 5);
$mysqli->real_connect($config['db_host'],$config['db_user'],$config['db_password'],$config['db_name']); 
                    
$grid = new EditableGrid();
$mysqli->query("SET NAMES 'utf8'");
//$grid->addColumn('id', 'ID', 'integer', NULL, false);
$grid->addColumn('unit_id', 'Unit ID', 'string');
$grid->addColumn('location', 'Location', 'string');  
$grid->addColumn('thai_location', 'Thai location', 'string');  
$grid->addColumn('location', 'Location', 'string');  
$grid->addColumn('owner', 'Owner', 'string');  
$grid->addColumn('phone', 'Phone', 'string'); 
/* The column id_country and id_continent will show a list of all available countries and continents. So, we select all rows from the tables */
$grid->addColumn('action', 'Action', 'html', NULL, false, 'id');  

//Edit here
$mydb_tablename = (isset($_GET['db_tablename'])) ? stripslashes($_GET['db_tablename']) : 'owner';

error_log(print_r($_GET,true));

$query = 'SELECT * FROM '.$mydb_tablename ;
$queryCount = 'SELECT count(id) as nb FROM '.$mydb_tablename;
$mysqli->query("SET NAMES 'utf8'");
$totalUnfiltered =$mysqli->query($queryCount)->fetch_row()[0];
$total = $totalUnfiltered;

$page=0;
if ( isset($_GET['page']) && is_numeric($_GET['page'])  )
  $page =  (int) $_GET['page'];

$rowByPage=15;

$from= ($page-1) * $rowByPage;

if ( isset($_GET['filter']) && $_GET['filter'] != "" ) {
  $filter =  $_GET['filter'];
  $query .= '  WHERE unit_id like "%'.$filter.'%" OR location like "%'.$filter.'%" OR thai_location like "%'.$filter.'%"';
  $queryCount .= '  WHERE unit_id like "%'.$filter.'%" OR location like "%'.$filter.'%" OR thai_location like "%'.$filter.'%"';
  $total =$mysqli->query($queryCount)->fetch_row()[0];
}

if ( isset($_GET['sort']) && $_GET['sort'] != "" )
  $query .= " ORDER BY " . $_GET['sort'] . (  $_GET['asc'] == "0" ? " DESC " : "" );

$query .= " LIMIT ". $from. ", ". $rowByPage;

error_log("pageCount = " . ceil($total/$rowByPage));
error_log("total = " .$total);
error_log("totalUnfiltered = " .$totalUnfiltered);

$grid->setPaginator(ceil($total/$rowByPage), (int) $total, (int) $totalUnfiltered, null);
/* END SERVER SIDE */ 

error_log($query);
$result = $mysqli->query($query );
$mysqli->close();

// send data to the browser
$grid->renderJSON($result,false, false, !isset($_GET['data_only']));