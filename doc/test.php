<?php 
    session_start();
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<script src="./js/jquery.min.js"></script>

<link rel="stylesheet" href="./css/bootstrap.min.css" media="all">
<link rel='stylesheet' type='text/css' href='./css/style.css'>

<title>Session Monitor</title>

<link rel="stylesheet" type="text/css" href="dist/jquery.dataTables.min.css"/>	 
<script type="text/javascript" src="dist/jquery.dataTables.min.js"></script>

<style media="all" type="text/css">
	.round_image{
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		-webkit-box-shadow: #000 0 2px 10px;
		-moz-box-shadow: #000 0 2px 10px;
		box-shadow: #000 0 2px 10px;
	}
</style>
<style>
	@media (min-width: 1500px) {
			.container{
				width: 1600px;
			}
	}
</style>
  
</head>
<body>

<?php
    echo '<br>******* 0-$_SESSION ********</br>';

    if(isset($_SESSION['username'])){
        // echo '\$_SESSION_id= '.$_SESSION['id'];
        echo '<br>\$_SESSION_username= '.$_SESSION['username'];
        echo '<br>\$_SESSION_level= '.$_SESSION['level'];
        echo '<br>\$_SESSION_section= '.$_SESSION['section'];
        echo '<br>\$_SESSION_status= '.$_SESSION['status'];
        echo '<br>\$_SESSION_EGATID= '.$_SESSION['EGATID'];
        echo '<br>\$_SESSION_PWS= '.$_SESSION['PWS'];
        
        echo '<br>\$_SESSION_location= '.$_SESSION['location'];
        echo '<br>\$_SESSION_unit= '.$_SESSION['unit'];
        echo '<br>\$_SESSION_owner= '.$_SESSION['owner'];
        echo '<br>\$_SESSION_quiz_EGATID= '.$_SESSION['EGATID'];
        echo '<br>\$_SESSION_phone= '.$_SESSION['phone'];

        // $_SESSION["location"]=$row["thai_location"]; 
        // $_SESSION["unit"]=$row["unit_id"]; 
        // $_SESSION["owner"]=$row["owner"]; 
        // $_SESSION["phone"]=$row["phone"]; 
        // echo '<br>\$_SESSION_quiz_mail= '.$_SESSION["mail"];
        // echo '<br>\$_SESSION_quiz_name= '.$_SESSION['quiz_name'];
        // echo '<br>\$_SESSION_quiz_type= '.$_SESSION['quiz_type'];
        // echo '<br>\$_SESSION_quiz_time= '.$_SESSION['quiz_time'];
        // echo '<br>\$_SESSION_quiz_code= '.$_SESSION['quiz_code'];
        // echo '<br>\$_SESSION_quiz_number= '.$_SESSION['quiz_number'];
        // echo '<br>\$_SESSION_quiz_order= '.$_SESSION['quiz_order'];

        // unset ($_SESSION["username"]);
        // unset ($_SESSION["level"]);
        // unset ($_SESSION["status"]);
        // unset ($_SESSION["section"]);
        // unset ($_SESSION["EGATID"]);
        // unset ($_SESSION["PWS"]);          

    }
/*
    echo '<br><br>******* 0-TIME ********</br>';
    $date1 = '2009-11-12 12:09:08';
    $date2 = '2009-12-01 08:20:11';
    $ts1 = strtotime($date1);
    $ts2 = strtotime($date2);
    echo $seconds_diff = $ts2 - $ts1;
    
    echo '<br>********* 2 ******</br>';    
    $dStart = new DateTime('2012-07-26');
    $dEnd  = new DateTime('2012-08-26');
    $dDiff = $dStart->diff($dEnd);
    echo $dDiff->format('%R'); // use for point out relation: smaller/greater
    echo $dDiff->days;

    echo '<br>********* 3 ******</br>';
    $str = "Jul 02 2013";
    $str = strtotime(date("M d Y ")) - (strtotime($str));
    echo floor($str/3600/24);
 
    echo '<br>********* 4 ******</br>';
    $date1=date_create("2013-03-15");
    $date2=date_create("2013-12-12");
    $diff=date_diff($date1,$date2);
    
    echo '<br>********* 5 ******</br>';
    $datetime1 = new DateTime('2009-10-11');
    $datetime2 = new DateTime('2009-10-13');
    $interval = $datetime1->diff($datetime2);
    echo $interval->format('%R%a days');

    echo '<br>********* 6 ******</br>';
    $datetime1 = date_create('2009-10-11');
    $datetime2 = date_create('2009-10-13');
    $interval = date_diff($datetime1, $datetime2);
    echo $interval->format('%R%a days');

    echo '<br>********* 7 ******</br>';
    $target_date= date('2019-03-28');
    echo '<br>\$target_date= '.$target_date;
    $current_date= date('Y-m-d');
    echo '<br>\$current_date= '.$current_date;
    $diffdate=strtotime($target_date)-strtotime($current_date);
    echo '<br>\$diffdate= '.$diffdate;

    $diffdat=$interval->format('%R%a days');
    if($diffdat >= 0){
        echo '<br>Diff date= '.$diffdat;
    }  
    
    echo '<br>********* 8 ******</br>';
    $current_date= date('Y-m-d');
    $q_date1=date('2019-03-27');
    $q_date2=date('2019-03-28');
    $diffdate1=strtotime($current_date)-strtotime($q_date1);
    $diffdate2=strtotime($q_date2)-strtotime($current_date);

    if($diffdate1 >= 0 && $diffdate2 >=0){
        echo '<br>OK...\$diffdate1= '.$diffdate1;
        echo '<br>OK...\$diffdate2= '.$diffdate2;
    }else{
        echo '<br>NOT OK...\$diffdate1= '.$diffdate1;
        echo '<br>NOT OK...\$diffdate2= '.$diffdate2;
    }

    echo '<br>********* Show time ******</br>';
    $showtime = date("Y")."/".date("m")."/".date("d")." ".date("H:i:s");
    echo ' <SCRIPT LANGUAGE="JavaScript">beforeload = (new Date()).getTime();</script>';
    echo ' <SCRIPT LANGUAGE="JavaScript">servertime = ' . $showtime . ';</SCRIPT>'  ;

    echo ' <SCRIPT LANGUAGE="JavaScript">
    afterload = (new Date()).getTime(); 
    seconds = (afterload-beforeload)/1000; 
    document.write("Browser Speed: " + seconds + " s"); 
    total=servertime + seconds;
    document.write("Total:" + total + " s");
    </script> ';

    echo '<br><br>*************random-1*************<br>';
    
    $input = array("11", "12", "13");
    $rand_keys = array_rand($input, 2);
    echo $input[$rand_keys[0]] . "\n";
    echo $input[$rand_keys[1]] . "\n";

    echo '<br><br>*************random-1*************<br>';
    
    $input = array("11", "12", "13");
    echo $q_type = $input[mt_rand(0, count($input) - 1)];
    */
?>

</body>
</html>