<?php /**By Anek suriwongyai 01-05-2562 */ 
    session_start();
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<script src="./js/jquery.min.js"></script>

<link rel="stylesheet" href="./css/bootstrap.min.css" media="all">
<link rel='stylesheet' type='text/css' href='./css/style.css'>
<title>LockOut TagOut (Tag Information)</title>
<link rel="stylesheet" type="text/css" href="dist/jquery.dataTables.min.css"/>	 
<script type="text/javascript" src="dist/jquery.dataTables.min.js"></script>

<style media="all" type="text/css">
	.round_image{
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		-webkit-box-shadow: #000 0 2px 10px;
		-moz-box-shadow: #000 0 2px 10px;
		box-shadow: #000 0 2px 10px;
	}
</style>

<style>
	@media (min-width: 500px) {
			.container{
				width: 800px;
                align-content: center;
			}
	}
</style>
 
</head>
<body>
<?php 
    require_once("navbar_index.php");
    require_once("connect_db.php");

    $http_client_ip = @$_SERVER['HTTP_CLIENT_IP'];
    $http_x_forwarded_for = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote_addr = $_SERVER['REMOTE_ADDR'];
    if(!empty($http_client_ip)){
        $ip_address = $http_client_ip;
    }elseif(!empty($http_x_forwarded_for)){
        $ip_address = $http_x_forwarded_for;
    }else{
        $ip_address = $remote_addr;
    }
    $hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);		
    $clientip = $ip_address.'['.$hostname.']';
    $gdate = date("Y-m-d H:i:s");

    if(isset($_POST['submit'])){
        $user = trim($_POST['username']) ;
        $pws = $mysqli->real_escape_string($_POST['password']) ; 
        // echo "<br>user=".$user.' '.$pws;
        // echo '<br>';
        // $sql="SELECT * FROM `user` WHERE `username` = '".$user."' AND `password` = '".$pws."'";
        $sql="SELECT * FROM `user` WHERE `egat_id` = '".$user."' AND `password` = '".$pws."'";
        $result=$mysqli->query($sql);
        if($result->num_rows>0){
            $row=$result->fetch_assoc();
            $_SESSION['id'] = "";
            // $_SESSION['fullname']=$row['name']; //ชื่อ-สกุล
            $_SESSION['username'] = $row['name'];
            $_SESSION['level'] = $row['level'] ;
            $_SESSION['status'] = $row['status'];
            $_SESSION['section'] = $row['section_id'];
            $_SESSION['EGATID'] = $user;
            $_SESSION['PWS'] = $pws;
            
            $sql = "insert into userlog (name, logintime, loginip) values ('$user', '$gdate', '$clientip')";
            $mysqli->query("SET NAMES 'utf8'");
            $mysqli->query($sql);

            header('location:index.php');
        }else{
            // $_SESSION['username'] = "";
            // $_SESSION['level'] = "" ;
            // $_SESSION['status'] = "";
            // $_SESSION['section'] = "";
            // $_SESSION['EGATID'] = "";
            // $_SESSION['PWS'] = "";
            unset ($_SESSION["username"]);
            unset ($_SESSION["level"]);
            unset ($_SESSION["status"]);
            unset ($_SESSION["section"]);
            unset ($_SESSION["EGATID"]);
            unset ($_SESSION["PWS"]);          
?>          
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 mx-auto mt-15">
                        <div class="card bg-warning">
                            <div class="card-header text-center bg-warning">
                                <h2><small>LOGIN FAIL...!</small></h2>                                
                            </div>
                            <div class="card-body text-center">                        
                                <h3>Invalid User name or Password..!</h3> 
                            </div>
                            <div class="card-footer text-center bg-warning">
                            <?php 
                                $url="login.php";
                                echo '<img src="images/process.gif"/><br/>' ;	
                                echo '<meta http-equiv="refresh" content="2; URL='.$url.'">' ;
                            ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php 
        }
    }
?>
</body>
</html>