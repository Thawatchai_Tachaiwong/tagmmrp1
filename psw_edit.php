<?php   /**By Anek suriwongyai 08-06-2562 */
	session_start();
    require_once("getowner.php");
    if(ISSET($_SESSION['EGATID']) && $_SESSION['PWS']==""){
        echo '<br><p align="center">Access Denied...!<br>You don\'t have permission to access on this page.<br>Please contact admin.<br>';
        echo 'To continous <a href="./login.php">Log on</a>';
        exit();
    }

?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<script src="./js/jquery.min.js"></script>

<link rel="stylesheet" href="./css/bootstrap.min.css" media="all">
<link rel='stylesheet' type='text/css' href='./css/style.css'>
<title>Password edit</title>
<link rel="stylesheet" type="text/css" href="dist/jquery.dataTables.min.css"/>	 
<script type="text/javascript" src="dist/jquery.dataTables.min.js"></script>

<style media="all" type="text/css">
	.round_image{
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		-webkit-box-shadow: #000 0 2px 10px;
		-moz-box-shadow: #000 0 2px 10px;
		box-shadow: #000 0 2px 10px;
	}
</style>
<style>
	@media (min-width: 1500px) {
			.container{
				width: 1600px;
			}
	}
</style>
  
</head>

<body>

<?php require_once("navbar_index.php");?>
<br>
<div class="container">
    <div class="row row-no-gutters mx-auto">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body text-left">
                    <div class="card-header bg-light text-dark"><h3>Change User Password</h3></div>
                    <form action="pswedit_act.php" method="POST">
                        <fieldset>
                            <div class="form-group">
                                <label for="username">User name</label>
                                <input type="text" id="username" name="username" class="form-control" value="<?php echo $_SESSION['EGATID'];?>" disabled>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="text" id="password" name="password" class="form-control" value="<?php echo $_SESSION['PWS'];?>" required>
                            </div>

                            <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
</div>
</body>
</html>