<?php
    session_start();
    require_once("includes/function.php");
    $_SESSION['starttime'] = '';
    $_SESSION['endtime'] = '';

    if(ISSET($_POST["submit"])){
        $sdate=$_POST["mydate1"];
        if($sdate==""){
            $current_date= date('Y-m-d');
            $_SESSION['starttime'] = $current_date;
        }else{
            $_SESSION['starttime'] = thaiDate($sdate);
        }
        $edate=$_POST["mydate2"];
        if($edate==""){
            $date = new DateTime("now", new DateTimeZone('Asia/Bangkok') );
            // $cDate= $date->format('Y-m-d H:i:s'); 
            $cDate = $date->format('Y-m-d'); 
            $date = strtotime($cDate);
            // $date = strtotime("-1 minute", $date);
            $end_time = date('Y-m-d', $date);
            $_SESSION['endtime'] = $end_time;
        }else{
            $_SESSION['endtime'] = thaiDate($edate);
        }
    }else{
            $_SESSION['starttime'] = '';
            $_SESSION['endtime'] = '';
    }

    // echo '<br>start date = '.$sdate;
    // echo '<br>start dates = '.$_SESSION['starttime'];
    // echo '<br>edate date = '.$edate;
    // echo '<br>edate dates = '.$_SESSION['endtime'];

?>

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Maemoh Tag Form MF-00-ASC-37-01-02</title>
<link href='css/style.css' rel='stylesheet' type='text/css'>
<script src="js/jquery.min.js"></script>  

<!-- <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script> -->
<!-- <link rel="stylesheet" href="css/jquery.datetimepicker.css"> -->


<!--*********Start calendar************-->
<link type="text/css" href="jquery/flora.calendars.picker.css" rel="stylesheet"/> 
<script type="text/javascript" src="jquery/jquery.min.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.min.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.plus.min.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.picker.min.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.thai.min.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.thai-th.js"></script> 
<script type="text/javascript" src="jquery/jquery.calendars.picker-th.js"></script> 



</head>
<body>

<script type="text/javascript">
    $(document).ready(function() {
        $("#results" ).load( "fetch_mf.php"); //load initial records
        
        //executes code below when user click on pagination links
        $("#results").on( "click", ".pagination a", function (e){
            e.preventDefault();
            $(".loading-div").show(); //show loading element
            var page = $(this).attr("data-page"); //get page number from link
            $("#results").load("fetch_mf.php",{"page":page}, function(){ //get content from PHP page
                $(".loading-div").hide(); //once done, hide loading element
            });		
        });
    });
</script>


<div align="center">
<div class="loading-div"><img src="ajax-loader.gif" ></div>
<div id="results"><!-- content will be loaded here --></div>
<hr>


<script type="text/javascript"> 
    $(function() {     
        $('#mydate1').calendarsPicker({calendar: $.calendars.instance('thai','th')}); 
        $('#mydate2').calendarsPicker({calendar: $.calendars.instance('thai','th')}); 
    });
</script>
<form action="printform.php" method="post">
	&nbsp;Start time : <input type="text" name="mydate1" value="" id="mydate1" >&nbsp;End time : <input type="text" name="mydate2" value="" id="mydate2" >&nbsp;	
	<input type="submit" name="submit" value="Submit">
</form>
<br />

<br />

<br />
</div>

<!-- <script src="js/jquery.min.js"></script>   -->
<!-- <script src="js/jquery.datetimepicker.js"></script> -->
<script type="text/javascript" src="js/script.js"></script>

</body>
</html>
