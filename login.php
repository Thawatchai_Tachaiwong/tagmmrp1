<?php /**By Anek suriwongyai 11-05-2562 */ require_once("getowner.php");?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<script src="./js/jquery.min.js"></script>

<link rel="stylesheet" href="./css/bootstrap.min.css" media="all">
<link rel='stylesheet' type='text/css' href='./css/style.css'>
<title>Log on</title>
<link rel="stylesheet" type="text/css" href="dist/jquery.dataTables.min.css"/>	 
<script type="text/javascript" src="dist/jquery.dataTables.min.js"></script>

<style media="all" type="text/css">
	.round_image{
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		-webkit-box-shadow: #000 0 2px 10px;
		-moz-box-shadow: #000 0 2px 10px;
		box-shadow: #000 0 2px 10px;
	}
</style>
<style>
	@media (min-width: 1500px) {
			.container{
				width: 1600px;
			}
	}
</style>
  
</head>
<body>
<?php require_once("navbar_index.php");?>
<div class="container">
    <div class="row row-no-gutters mx-auto">
        <div class="col-md-2"></div>
        <div class="col-md-2">
            <div class="card">
                <div class="card-body text-left">
                    <div class="card-header bg-light text-dark"><h3>Log on</h3></div>
                    <form action="login_process.php" method="POST">
                        <fieldset>
                            <div class="form-group">
                                <label for="username">User name</label>
                                <input type="text" id="username" name="username" class="form-control" placeholder="egat id" required>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" id="password" name="password" class="form-control" placeholder="****" required>
                            </div>

                            <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8"></div>
    </div>
</div>
</body>
</html>