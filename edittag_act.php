
<?php    /**By Anek suriwongyai 11-05-2562 */
    session_start();
    require_once("includes/function.php");
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<script src="./js/jquery.min.js"></script>

<link rel="stylesheet" href="./css/bootstrap.min.css" media="all">
<link rel='stylesheet' type='text/css' href='./css/style.css'>
<title>Edit tag progress</title>
<link rel="stylesheet" type="text/css" href="dist/jquery.dataTables.min.css"/>	 
<script type="text/javascript" src="dist/jquery.dataTables.min.js"></script>

<style media="all" type="text/css">
	.round_image{
		-webkit-border-radius: 10px;
		-moz-border-radius: 10px;
		border-radius: 10px;
		-webkit-box-shadow: #000 0 2px 10px;
		-moz-box-shadow: #000 0 2px 10px;
		box-shadow: #000 0 2px 10px;
	}
</style>

<style>
	@media (min-width: 500px) {
			.container{
				width: 800px;
                align-content: center;
			}
	}
</style>
 
</head>
<body>

<?php
    require_once("navbar_index.php");

    $tbname="tag";
    // $id=$_POST["id"];
    $edituser=$_SESSION["EGATID"];
    $gdate=date("Y-m-d H:i:s");
    $clientip=$_SERVER["REMOTE_ADDR"];
    if($clientip=="::1"){
        $clientip="127.0.0.1";
    }

    if(ISSET($_POST['submit'])){
        $id=$_POST["id"];
        $url="tagdetail.php?id=".$id;
        if($_SESSION["status"]=='M'){
            $update = "UPDATE $tbname SET aks='".$_POST["aks"]."', tagm_date1='".$_POST["tagm_date1"]."', tagm_h1='".$_POST["tagm_h1"]."', task='".$_POST["task"]."', tagm_editby='".$edituser."', tagm_edittime='".$gdate."', tagm_editip='".$clientip."' WHERE id='$id'";
        }

        if($_SESSION["status"]=='O'){
                $tago_h2=isset($_POST["tago_h2"])?$_POST["tago_h2"]:"";
                $tagm_no=isset($_POST["tagm_no"])?$_POST["tagm_no"]:"";
                if ($tago_h2!=""){
                    if ($tagm_no!=""){
                        $update = "UPDATE $tbname SET aks='".$_POST["aks"]."', tagm_date1='".$_POST["tagm_date1"]."', tago_date1='".$_POST["tago_date1"]."', tagm_h1='".$_POST["tagm_h1"]."', tago_h1='".$_POST["tago_h1"]."', tagmo_date2='".$_POST["tagmo_date2"]."', tagm_h2='".$_POST["tagm_h2"]."', tago_h2='".$_POST["tago_h2"]."',task='".$_POST["task"]."', tago_editby='".$edituser."', tago_edittime='".$gdate."', tago_editip='".$clientip."' WHERE id='$id'";	
                    }else{
                        $update = "UPDATE $tbname SET aks='".$_POST["aks"]."', tago_date1='".$_POST["tago_date1"]."', tago_h1='".$_POST["tago_h1"]."', tagmo_date2='".$_POST["tagmo_date2"]."', tago_h2='".$_POST["tago_h2"]."',task='".$_POST["task"]."', tago_editby='".$edituser."', tago_edittime='".$gdate."', tago_editip='".$clientip."' WHERE id='$id'";
                    }
                }else{
                    if ($tagm_no!=""){
                        $update = "UPDATE $tbname SET aks='".$_POST["aks"]."', tagm_date1='".$_POST["tagm_date1"]."', tago_date1='".$_POST["tago_date1"]."', tagm_h1='".$_POST["tagm_h1"]."', tago_h1='".$_POST["tago_h1"]."', task='".$_POST["task"]."', tago_editby='".$edituser."', tago_edittime='".$gdate."', tago_editip='".$clientip."' WHERE id='$id'";
                    }else{
                        $update = "UPDATE $tbname SET aks='".$_POST["aks"]."', tago_date1='".$_POST["tago_date1"]."', tago_h1='".$_POST["tago_h1"]."', task='".$_POST["task"]."', tago_editby='".$edituser."', tago_edittime='".$gdate."', tago_editip='".$clientip."' WHERE id='$id'";
                    }
                }
        }
        include_once("connect_db.php");
        $mysqli->query("SET NAMES 'utf8'");
        $result = $mysqli->query($update);
        $affected_rows = $mysqli->affected_rows;
        $mysqli->close();

        if ( @$affected_rows == 1 ) {
            $msg="Update data successful...";
            echo '<center><br /><br /><table width="60%" border="0" cellspacing="0"><tr bgcolor="#eee" height="110"><td align="center">';
            echo '<br/><font face="MS Sans Serif"><font color="red" size="+1">'.$msg.'</font></font><br/>' ;
            echo '<img src="images/process.gif"/><br/>' ;	
            echo '<meta http-equiv="refresh" content="2; URL='.$url.'">' ;
            echo '</tr></td></table></center>';
        }else{
            $msg="Can not update data...!";
            echo '<center><br /><br /><table width="40%" border="0" cellspacing="0"><tr bgcolor="#eee" height="110"><td align="center">';
            echo '<br/><font face="MS Sans Serif"><font color="red" size="+1">'.$msg.'</font></font><br/>' ;
            echo '<img src="images/process.gif"/><br/>' ;	
            echo '<meta http-equiv="refresh" content="2; URL='.$url.'">' ;
            echo '</tr></td></table></center>';
        }
    }
?>

</body>
</html>